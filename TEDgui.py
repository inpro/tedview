# -*- coding: utf-8 -*-
# see the file COPYING for license details!
# Python stdlibs:
import sys
import time
import thread
import Tkinter
import Tkdnd
import tkFileDialog
import tkMessageBox
import tkSimpleDialog
from Tkconstants import PAGES, UNITS, NORMAL, RAISED, SUNKEN, HORIZONTAL, RIGHT, BOTH, LEFT, BOTTOM, TOP, NW, HIDDEN, X, Y, ALL
import Queue
import math
import socket

# pickle
import pickle

# 3rd-party libs:
import tkSnack

# TEDview stuff:

import datasource
import auxiliary_tools as tools
from TEDmodel import *
from TEDtypes import *
import TEDxmlParser
import TEDtgxmlconverter

# FIXME:
height_label = 30

class Bookmark(Tkinter.Frame):

    def __init__(self, controller):

        self.controller = controller
        self.bookmarks = {}

        self.entryCount = 0

        self.tk_bookmarkList = Tkinter.Toplevel()
        self.tk_bookmarkList.title("Bookmarks")

        name = Tkinter.Label(self.tk_bookmarkList)
        name.text = "Name"
        self.ui_list_name = Tkinter.Listbox(self.tk_bookmarkList)
        self.ui_list_name.insert(0, "Name")

        name.grid(row=0, column=1)
        self.ui_list_name.grid(row=1, column=1)

        self.ui_list_starttime = Tkinter.Listbox(self.tk_bookmarkList)
        self.ui_list_starttime.insert(0, "Start Time")
        self.ui_list_starttime.grid(row=1, column=2)
        
        self.ui_list_comment = Tkinter.Listbox(self.tk_bookmarkList)
        self.ui_list_comment.insert(0, "Comments")
        self.ui_list_comment.grid(row=1, column=3)

        # plus one empty entry as spacing
        self.ui_list_name.insert(1,"")
        self.ui_list_starttime.insert(1,"")
        self.ui_list_comment.insert(1,"")

        self.tk_bookmarkList.bind("<Button-3>", self.__rightClickMenu__)
        self.tk_bookmarkList.bind("<Button-2>", self.__rightClickMenu__)
        self.tk_bookmarkList.bind("<Double-Button-1>", lambda e:self.get(self.__getMarkedEntryIndex__()))
        self.tk_bookmarkList.bind("<Delete>", lambda e:self.remove(self.__getMarkedEntryIndex__()))
        self.ui_list_comment.bind("<Button-1>", lambda e:self.__setComment__(self.__getMarkedEntryIndex__()))

        self.open_dialog = tkFileDialog.Open(self.tk_bookmarkList)
        self.save_dialog = tkFileDialog.SaveAs(self.tk_bookmarkList)
        
        self.menu = Tkinter.Menu(self.tk_bookmarkList)
        self.tk_bookmarkList.config(menu=self.menu)

        self.subMenu = Tkinter.Menu(self.menu)
        self.menu.add_cascade(label="File",menu=self.subMenu)
        
        self.subMenu.add_command(label="save bookmarks", command=lambda:self.saveBookmarksToFile(self.save_dialog.show()))
        self.subMenu.add_command(label="load bookmarks", command=lambda:self.loadBookmarksFromFile(self.open_dialog.show()))
        self.subMenu.add_separator()
        self.subMenu.add_command(label="Jump to", command=lambda:self.get(self.__getMarkedEntryIndex__()))
        self.subMenu.add_separator()
        self.subMenu.add_command(label="Remove", command=lambda:self.remove(self.__getMarkedEntryIndex__()))
        self.subMenu.add_command(label="Remove all", command=self.removeAll)
        self.subMenu.add_separator()

    def __rightClickMenu__(self, event):
        self.menu = Tkinter.Menu(self.tk_bookmarkList, tearoff=0)

        selectedIndex = self.__getMarkedEntryIndex__()
        
        self.menu.add_command(label="Jump to", command=lambda:self.get(selectedIndex))
        self.menu.add_command(label="Delete", command=lambda:self.remove(selectedIndex))
        
        try:
            self.menu.post(event.x_root, event.y_root)
        except:
            print "Error. Sorry, something went wrong while trying to post the menu."


    def __getMarkedEntryIndex__(self):
        selectedIndex = None
        
        selectedItem = list(self.ui_list_name.curselection())
        
        if selectedItem == []:
            selectedItem = list(self.ui_list_starttime.curselection())
        if selectedItem == []:
            selectedItem = list(self.ui_list_comment.curselection())

        if len(selectedItem) > 1:
            "More than one selected item. Warn user"
            selectedIndex = None
        elif len(selectedItem) == 0:
            "Nothing Selected."
            selectedIndex = None
        elif selectedItem == [0]:
            "Marked the table header"
            selectedIndex = None
        else:
            selectedIndex = int(selectedItem.pop())

        if selectedIndex > 1:
            return selectedIndex
        else:
            return None

    def __setComment__(self, idx):
        if idx:
            comment = self.__promptComment__()

            try:
                self.ui_list_comment.delete(idx)
                self.ui_list_comment.insert(idx, comment)
                
                for name in self.bookmarks:
                    start, i, _ = self.bookmarks[name]
                    if i == idx:
                        self.bookmarks[name] = (start, i, comment)
                        #print "DEBUG added ", name, ",", str(start), ",", str(i), ",", comment
            except:
                # Occurs when nothing is selected and someone clicks. Or someone is trying to edit the headline of the column.
                pass

    def add(self, start, __name__=None, __comment__="Click to comment"):


        if start == None:
            return

        if __name__ == None:
            name = self.__promptName__()

            if name == "default":
                name+= "_"+str(start)
                        
        else:
            name = __name__

        if name in self.bookmarks:
            self.remove(name)

        columnNumber = self.entryCount+2
        self.bookmarks[name] = (start, columnNumber, __comment__)
        
        self.ui_list_name.insert(columnNumber, name)
        self.ui_list_starttime.insert(columnNumber, str(start))
        # TODO: "Click here to write your comment" -> Textfield
        self.ui_list_comment.insert(columnNumber, __comment__)

        self.entryCount += 1

    def remove(self, index):
        if index == None:
            name = self.__promptNameNoneSelected__()
            time, index, comment = self.bookmarks[name]

        self.ui_list_name.delete(index)
        self.ui_list_starttime.delete(index)
        self.ui_list_comment.delete(index)

        self.entryCount -= 1
        
        nameToDel = ""

        for entry in self.bookmarks:
            time, i, _ = self.bookmarks[entry]
            if i == index:
                nameToDel = entry
                
        del self.bookmarks[nameToDel]

    def get(self, index):

        if index == None:
            return
            #name = self.__promptNameNoneSelected__()
            try:
                time, i, _ = self.bookmarks[name]
                self.controller.jumpToBookmark(time)
                return
            except KeyError:
                print "Unknown Bookmark. Please doublecheck the bookmark name."

        for entry in self.bookmarks:
            time, i, _ = self.bookmarks[entry]
            if i == index:
                self.controller.jumpToBookmark(time)
                return
            
        print "Something went wrong. Index out of range in Bookmarks"
        return


    def __promptNameNoneSelected__(self):

        result = tkSimpleDialog.askstring("Bookmark", "No bookmark selected, please enter a bookmark name")
        return result
    
    def __promptName__(self):

        result = tkSimpleDialog.askstring("Bookmark", "Please enter a name, or RETURN to default")
        if result == "":
            result = "default"
        
        return result

    def __promptComment__(self):
        result = tkSimpleDialog.askstring("Bookmark", "Please write your comment")
        return result

    def removeAll(self):
        temp = []
        for entry in self.bookmarks:
            time, idx, comment = self.bookmarks[entry]
            temp.append(idx)

        for entry in temp:
            self.remove(entry)

    def saveBookmarksToFile(self, filename):
        fileObj = open(filename, "w+b")
        pickle.dump((self.bookmarks, self.controller.openedFileName),fileObj)

    def loadBookmarksFromFile(self, filename):
        self.removeAll()
        try:
            fileObj = open(filename)
            loadedBookmarks, loadedFileName = pickle.load(fileObj)

            if loadedFileName != self.controller.openedFileName:
                print "Warning. Your bookmarks might belong to different XML data!"

            for name in loadedBookmarks:
                time, idx, comment = loadedBookmarks[name]
                self.add(time, name, comment)
            fileObj.close()        
        except:
            print "Sorry. Something went wrong while trying to open the saved Bookmarks"

    
###############################################################################
#       Controller:                                                           #
###############################################################################

class Controller:

    """
    The C in MVC (model view controller patter, see Gamma 1995).  It
    provides a central interface for the GUI which can be used for example by
    events triggered by button presses and mouse actions.
    """
    
    def __init__(self, tk_root, ipaddress, port, vid_offset, vidPort=None):
        self.ipaddress = ipaddress
        self.port = port
        self.vid_offset = vid_offset
        self.tracker = Tracker()
        self.tk_root = tk_root
        self.main_frame = MainFrame(tk_root, self)
        self.main_frame.pack(fill=BOTH, expand=1)
        self.tracker_pane = self.main_frame.tracker_pane
        self.new_events = Queue.Queue()
        self.main_frame.after(100, self.update)

        self.openedFileName = None

        self.xmlP = TEDxmlParser.XMLParser()
        self.tgxmlConverter = TEDtgxmlconverter.Converter()

        #self.tk_relationsGraph = Tkinter.Toplevel()
        #self.tk_relationsGraph.title("SLL/IG-Relations")

        # starting a seperate window for bookmarks
        self.bookmark = None

        self.rightClickStart = (None,None)
        
        if vidPort:        
            self.vidSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.vidSocket.connect((ipaddress, vidPort))
        else:
            self.vidSocket = None
        
        
    def enqueue_new_event(self, event):
        """
        Schedules a new event for integration.  This function is guarateed
        to work, when called from outside the main thread (i.e. the Tkinter
        thread).  (Unlike the others!) 
        """
        
        self.new_events.put(event)
        
        
    def update(self):
        """ 
        This function is called periodically by the main thread (i.e. the
        GUI).  It processes input from other threads.  If input queues are
        empty it does nothing.
        """
        
        while not self.new_events.empty():
            
            event = self.new_events.get_nowait()

            if type(event) == DataEvent:
                self.tracker.add_event(event)
                
            elif type(event) == ControlEvent:
            
                if event.action == 'new':
                    tools.log("new channel ...")
                    self.tracker.new_channel(event.originator)
                elif event.action == 'clear':
                    tools.log("clearing channel ...")
                    self.tracker.clear_channel(event.originator)
                else:
                    tools.log("unknown action: " + event.action)
                    
        self.tracker_pane.ruler.update()
        self.main_frame.update_idletasks()
        self.main_frame.after(100, self.update)
    
    # The remainder consists of event handlers.
    def open_file(self, filename):
        """ 
        Opens the specified XML file and feeds the contained events to the
        GUI.
        """

        fileTypeName = filename.split(".")[-1].lower()
        self.openedFileName = filename
        if filename:
                        
            parsedEvents = []
            xmlstring = ""
            
            if fileTypeName == "xml":
                fStream = open(filename)
                xmlstring = fStream.read()
                fStream.close()
            elif fileTypeName == "textgrid":            
                xmlstring = self.tgxmlConverter.convertToXml(filename)
            else:
                print "Sorry. I do not know the type of the file you provided."

            parsedEvents = self.xmlP.process_xml(xmlstring)
            
            for event in parsedEvents:
                self.enqueue_new_event(event)

                    
    def save_as_file(self, filename):
        """ 
        Saves the currently displayed data as TEDview XML
        """
        
        if filename:
            f = open(filename, 'w')
            print >>f, self.tracker.serialize_to_xml()
            f.close()
            
    def new_inspector(self):
        InspectorFrame(self.tk_root, self.tracker)
        
    def settings(self):
        SettingsFrame(self.tk_root, self)
        
    def remove_channels(self):
        """ 
        Clears and removes all current channels.
        """
        for channel in self.tracker.get_channels():
            self.tracker.remove_channel(channel)
            
    def zoom_out(self):
        """
        Zooms by factor 0.5, i.e. out.
        """
        self.tracker_pane.zoom(0.5)
        
    def zoom_in(self):
        """
        Zooms by factor 2.0, i.e. in.
        """
        self.tracker_pane.zoom(2.0)
        
    def setBookmark(self, time):
        """
        Add bookmark to jump to.
        """

        # move this to Bookmark, where it belongs
        if self.bookmark == None:
            self.bookmark = Bookmark(self)
        self.bookmark.add(time)
        #print "saved bookmark at "+str(time)

    def jumpToBookmark(self, time):
        self.tracker_pane.center_to(time)
        self.main_frame.lift()

    def setRightClickStart(self, event):
        self.rightClickStart = event.x, event.y
            

    def getRightClickStart(self):
        if self.rightClickStart == (None,None):
            return None
        return self.rightClickStart

    def loopmark(self, event):
        xStart, yStart = self.getRightClickStart()
        xEnd, yEnd = event.x, event.y

        if xStart > xEnd:
            a = xStart
            xStart = xEnd
            xEnd = xStart
        else:
            pass

        self.tracker_pane.userSetArea = True

        startTime = self.tracker_pane.coordinateToTime(xStart)
        endTime = self.tracker_pane.coordinateToTime(xEnd)

        # change buttons as in toggle_play
        if self.tracker_pane.player_playing:
            pass
        else:
            self.main_frame.buttons["toggle play"]["text"] = "stop"
        
        self.tracker_pane.userSetAreaStart = startTime
        self.tracker_pane.userSetAreaEnd = endTime
        self.tracker_pane.play(startTime, endTime)

    def toggle_play(self, event=None):
        """ 
        Toggles play mode from playing to pause or the other way around. 
        """
        
        if self.tracker_pane.player_playing:
            self.tracker_pane.stop()
            self.main_frame.buttons["toggle play"]["text"] = "play"
            # refactor the vp commands into stop and play, where they belong to
            # also removes the necessity of sending vp commands on loopback

            #print "stopped at: " + str(self.tracker.current_time)
            if self.vidSocket:
                self.vidSocket.send("stopped at: " + str(self.tracker.current_time))
                self.vidSocket.send("stop")
            
        else:
            if self.tracker.current_time < self.tracker.get_duration():
                self.tracker_pane.play()
                self.main_frame.buttons["toggle play"]["text"] = "stop"
                # print info about start time, for controlling
                # video player:
                # same as above, move to play and stop
                #print "started at: " + str(self.tracker.current_time)
                if self.vidSocket:
                    self.vidSocket.send("started at: " + str(self.tracker.current_time))
                print int(self.tracker.current_time - (self.vid_offset * 1000)), " ", str(self.tracker_pane.speed)

    def resetPlayingOptions(self):
        pass

    def resetZoom(self):
        pass
    
    def speed_up(self):
        """ 
        Increases playback rate. 
        """
        
        # TODO: Start / stop autio playback as neccesary.
        #self.tracker_pane.accelerate(2.0)
        self.tracker_pane.accelerate(1.25)
        self.main_frame.speed_tkvar.set(self.tracker_pane.speed)
        
    def speed_down(self):
        """ 
        Decreases playback rate. 
        """
        
        #self.tracker_pane.accelerate(0.5)
        self.tracker_pane.accelerate(0.8)
        self.main_frame.speed_tkvar.set(self.tracker_pane.speed)
        
    def step(self, value, unit):
        """ 
        Shifts the dialogue view by a specified ammount.  value and unit
        work exactly as with scrolling of Tkinter views, i.e. unit can be
        Tkconstants.PAGES or TKconstants.UNITS.  So with value=-1 and
        unit=PAGES we scroll one page back.  1 unit is a tenth of a
        screenwidth. 
        """
        self.tracker_pane.xview('scroll', value, unit)
        
    def seek(self, position):
        """ 
        Seeks to a position in the dialogue.  Valid values for position are
        between 0, which is the start of the dialogue, to 1, which is the end.
        """
        self.tracker_pane.xview('moveto', position)        

###############################################################################
#       Widgets:                                                              #
###############################################################################

class MainFrame(Tkinter.Frame):
    """ The MainFrame is the main window which contains the rest of the GUI. """
    def __init__(self, master, controller, **opts):

        Tkinter.Frame.__init__(self, master, **opts)
        master.title("TEDview")
        self.controller = controller
        # Menubar:
        self.menu = Tkinter.Menu(master)
        self.master.config(menu=self.menu)
        self.fileMenu = Tkinter.Menu(self.menu)
        self.viewMenu = Tkinter.Menu(self.menu)
        self.playMenu = Tkinter.Menu(self.menu)
        
        self.open_dialog = tkFileDialog.Open(master)
        self.save_dialog = tkFileDialog.SaveAs(master)

        self.menu.add_cascade(label="File", menu=self.fileMenu)
        self.menu.add_cascade(label="View", menu=self.viewMenu)
        self.menu.add_cascade(label="Play", menu=self.playMenu)
        
        self.fileMenu.add_command(label="open",
            command=lambda:controller.open_file(self.open_dialog.show()))
        self.fileMenu.add_command(label="save as", 
            command=lambda:controller.save_as_file(self.save_dialog.show()))
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="clear", command=self.controller.remove_channels)
        self.fileMenu.add_separator()
	self.fileMenu.add_command(label="quit", command=self.quit)


        self.viewMenu.add_command(label="inspect", command=self.controller.new_inspector)
        self.viewMenu.add_command(label="New Bookmarks window", command=self.launchNewBookmarkWindow)

        self.playMenu.add_command(label="Zoom in", command=self.controller.zoom_in)
        self.playMenu.add_command(label="Zoom out", command=self.controller.zoom_out)
        self.fileMenu.add_separator()
        self.playMenu.add_command(label="Slow down", command=self.controller.speed_down)
        self.playMenu.add_command(label="Speed up", command=self.controller.speed_up)
        #self.playMenu.add_command(label="Reset zoom", pass)
        #self.playMenu.add_command(label="Reset speed", pass)

        # TODO: add settings window
        #self.menu.add_command(label="settings", command=controller.settings)
	# TODO: add the channels to the view-cascade and connect them to show/hide
#        self.viewmenu = Tkinter.Menu(master)
#        self.menu.add_cascade(label="show", menu=self.viewmenu)
        # TODO:
        #self.menu.add_command(label="about", command=...)
        master.config(menu=self.menu)
        # Tracker view:
        self.data_view_pane = Tkinter.Frame(self)
        self.channel_names_pane = Tkinter.Frame(self.data_view_pane)
        self.tracker_pane = TrackerPane(self, self.data_view_pane, controller,
                                        bg="yellow")
        self.tracker_pane.pack(side=RIGHT, fill=BOTH, expand=1)

        self.channel_names_pane.pack(side=LEFT, fill=Y, expand=0)
        Tkinter.Label(self.channel_names_pane, text=" ").pack(side=BOTTOM)
        self.scrollbar = Tkinter.Scrollbar(self.tracker_pane, name="scrollbar",
                                           orient=HORIZONTAL)
        self.tracker_pane.xscrollcommand(self.scrollbar.set)
        self.scrollbar['command'] = self.tracker_pane.xview
        # Toolbar:
        self.toolbar = Tkinter.Frame(self)
        self.buttons = (
            ("zoom in", Tkinter.Button(self.toolbar, bd=1, text="zoom in",
                                command=controller.zoom_in, background="grey")),
            ("zoom out", Tkinter.Button(self.toolbar, bd=1, text="zoom out",
                               command=controller.zoom_out, background="grey")),
            ("speed down", Tkinter.Button(self.toolbar, bd=1, text="-",
                              command=controller.speed_down,background="grey")),
            ("toggle play", Tkinter.Button(self.toolbar, bd=1, text="play",
                            command=controller.toggle_play, background="grey")),
            ("speed up", Tkinter.Button(self.toolbar, bd=1, text="+",
                                command=controller.speed_up,background="grey")),
        )
        for _, b in self.buttons:
            b.pack(side=LEFT, padx=0, pady=0)
        self.buttons = dict(self.buttons)
        # Ruler:
        container = Tkinter.Frame(self.channel_names_pane, relief=SUNKEN,
                                  bd=1, padx=3, height=20)
        vbar = Tkinter.Canvas(container, height=20, width=1)
        vbar.pack(side=LEFT)
        label = Tkinter.Label(container, text="time (mm:ss)")
        label.pack(fill=X, expand=0)
        container.pack(fill=X, expand=0, side=TOP)
        self.tracker_pane.add_ruler()
        # Statusbar:
        self.status_bar = Tkinter.Frame(self)
        frame = Tkinter.Frame(self.status_bar, bd=1, relief=SUNKEN)
        frame.pack(side=LEFT, padx=1, pady=2)
        # position:
        Tkinter.Label(frame, text="Time:").pack(side=LEFT)
        Tkinter.Label(frame,
                      textvariable=controller.tracker.current_time_tkvar).pack(side=LEFT)
        #Tkinter.Label(frame, text="ms").pack(side=LEFT)
        # scale factor:
        frame = Tkinter.Frame(self.status_bar, bd=1, relief=SUNKEN)
        frame.pack(side=LEFT, padx=1, pady=2)
        Tkinter.Label(frame, text="1px ≙").pack(side=LEFT)
        Tkinter.Label(frame,
                      textvariable=self.tracker_pane.scale_factor_tkvar).pack(side=LEFT)
        Tkinter.Label(frame, text="ms").pack(side=LEFT)
        # speed:
        frame = Tkinter.Frame(self.status_bar, bd=1, relief=SUNKEN)
        frame.pack(side=LEFT, padx=1, pady=2)
        Tkinter.Label(frame, text="Speed:").pack(side=LEFT)
        self.speed_tkvar = Tkinter.StringVar()
        self.speed_tkvar.set(self.tracker_pane.speed)
        Tkinter.Label(frame, textvariable=self.speed_tkvar).pack(side=LEFT)
        # no. of objects:
        frame = Tkinter.Frame(self.status_bar, bd=1, relief=SUNKEN)
        frame.pack(side=LEFT, padx=1, pady=2)
        Tkinter.Label(frame, text="#Objects:").pack(side=LEFT)
        Tkinter.Label(frame,
                      textvariable=controller.tracker.no_events_tkvar).pack(side=LEFT)
        # filler:
        frame = Tkinter.Frame(self.status_bar, bd=1, relief=SUNKEN)
        Tkinter.Label(frame, text=" ").pack(fill=X, expand=1)
        frame.pack(side=LEFT, padx=1, pady=2, fill=X, expand=1)

        self.status_bar.pack(side=BOTTOM, fill=X)
        self.toolbar.pack(side=BOTTOM)
        self.scrollbar.pack(fill=X, side=BOTTOM)
        #self.tracker_pane.pack(fill=BOTH, expand=1, side=BOTTOM)
        self.data_view_pane.pack(fill=BOTH, expand=1, side=BOTTOM)
        
        # Keybindings:
        # js: all bindings changed to bind_all. where you clicked last, shouldnt have an influence on the keybindings.
        # in addition to that, keybindings should be usable if other widgets are focused (bookmarks and inspectors)
        # are there downsides to bind_all? 
	# TIMO: Yes, the downside is that you can't type these in the save dialog
        self.bind_all('<space>', controller.toggle_play)
        self.bind_all('<Left>',  lambda e:controller.step(-1, UNITS))
        self.bind_all('<Right>', lambda e:controller.step(1, UNITS))
        self.bind_all('<Control-Left>',  lambda e:controller.step(-10, UNITS))
        self.bind_all('<Control-Right>', lambda e:controller.step(10, UNITS))
        self.bind_all('<Shift-Left>', lambda e:controller.step(-0.1, UNITS))
        self.bind_all('<Shift-Right>', lambda e:controller.step(0.1, UNITS))
        self.bind_all('<Prior>', lambda e:controller.step(-1, PAGES))
        self.bind_all('<Next>',  lambda e:controller.step(1, PAGES))
        self.bind_all('<Control-Prior>', lambda e:controller.step(-10, PAGES))
        self.bind_all('<Control-Next>',  lambda e:controller.step(10, PAGES))
        self.bind_all('<Shift-Prior>', lambda e:controller.step(-0.1, PAGES))
        self.bind_all('<Shift-Next>',  lambda e:controller.step(0.1, PAGES))
        self.bind_all('<Home>',  lambda e:controller.seek(0))
        self.bind_all('<End>',   lambda e:controller.seek(1))
#        self.bind_all('q',   lambda e:controller.zoom_out())
#        self.bind_all('w',  lambda e:controller.zoom_in())
#        self.bind_all('-',   lambda e:controller.zoom_out())
#        self.bind_all('+',  lambda e:controller.zoom_in())
        self.bind_all('f', lambda e:controller.speed_up())
        self.bind_all('s', lambda e:controller.speed_down())
        self.bind_all('<Control-q>', lambda e:master.destroy())

        #self.bind('<Button-3>', controller.setRightClickStart)
        # self.bind('<ButtonRelease-3>', controller.loopmark)
        # TODO: Understand why the wheel works only with bind_all.
        # TODO: js: i guess the clicks dont register, because the channel pane
        # is not on top of its frame .. bindall then binds on every possible layer
        self.bind_all('<4>', lambda e:controller.step(-1, UNITS))
        self.bind_all('<Shift-4>', lambda e:controller.step(-0.1, UNITS))
        self.bind_all('<Control-4>', lambda e:controller.step(-10, UNITS))
        self.bind_all('<5>', lambda e:controller.step(1, UNITS))
        self.bind_all('<Shift-5>', lambda e:controller.step(0.1, UNITS))
        self.bind_all('<Control-5>', lambda e:controller.step(10, UNITS))
        # das, 290509: if I can't get a menubar, then I at least
        #  want keybindings, damit! Or: I can haz inspectas too!
#        self.bind_all('o', lambda e:controller.open_file(self.open_dialog.show()))
#        self.bind_all('i', lambda e:controller.new_inspector())
#        self.bind_all('r', lambda e:controller.remove_channels())
#        self.bind_all('s', lambda e:controller.save_as_file(self.save_dialog.show()))
        # das: on OS X, the mousewheel generates a different event:
        self.bind_all('<MouseWheel>', lambda e:controller.step(e.delta*-1, UNITS))
        self.bind_all('<Shift-MouseWheel>', lambda e:controller.step(e.delta * -0.1, UNITS))
        self.bind_all('<Control-MouseWheel>', lambda e:controller.step(e.delta * -10, UNITS))
        self.focus_set()
        # Stores references to the labels of channels.  Needed for when
        # deleting channels.
        self.channel_labels = {}
        # Register as observer:
        self.controller.tracker.register_for_channels(self.handle_channel_change)
        # initialize
        self.channelSettingsDict = {}
        tkSnack.initializeSnack(self)

    def launchNewBookmarkWindow(self):
        self.controller.bookmark = Bookmark(self.controller)

    def handle_channel_change(self, action, channel):
        if action=="add":
            self.new_channel_pane(channel)
        elif action=="clear":
            pass
        elif action=="remove":
            self.remove_channel_pane(channel)
            
    def remove_channel_pane(self, channel):
# FIXME: remove menu entries
#        self.viewmenu.delete(self.viewmenu.index(1))
        self.channel_labels[channel.name].destroy()
        
    def new_channel_pane(self, channel):
        """ Creates the visual representation of a Channel, a ChannelPane.
        Such a ChannelPane consists of a label with the name of the channel and
        a track which shows the content of the channel. """
        container = ChannelSettings(self, self.channel_names_pane, channel)
        container.pack(fill=BOTH, expand=1, side=TOP)

        self.channelSettingsDict[channel.name] = container
        
        # FIXME: should be selected by default, add logic to actually hide the channel
#        self.viewmenu.add_checkbutton(label=channel.name)

        idx = len(self.channel_labels)
        self.channel_labels[channel.name] = (channel, idx) 
        #print self.channel_labels

        self.scrollbar.lift()
        self.update_idletasks()

class ChannelSettings(Tkinter.Frame):
    def __init__(self, mother, master, channel, **opts):
        
        Tkinter.Frame.__init__(self, master, relief=SUNKEN, bd=1, padx=3, height=45, **opts)
        self.channel = channel
        self.mother = mother
        self.master = master

        try:
            self.name = channel.name
        except AttributeError:
            self.name = channel

        vbar = Tkinter.Canvas(self, height=43, width=1)
        vbar.pack(side=LEFT)
        label = Tkinter.Label(self, text=self.name)
        label.bind('<Button-2>', self.remove)
        label.pack(side=LEFT, fill=BOTH, expand=1)

        moveDownButton = Tkinter.Label(self, text="move down", font=('Helvetica', 7))
        moveDownButton.bind('<Button-1>', self.moveDown)
        moveDownButton.pack(side=BOTTOM)
        #image=
        moveDownUp = Tkinter.Label(self, text="move up", font=('Helvetica', 7))
        moveDownUp.bind('<Button-1>', self.moveUp)
        moveDownUp.pack(side=TOP)

# TODO: add buttons to move channel up/down, 
# to remove or hide a channel,
# to call its inspector,
# maybe to vertically resize channel, and
# to mute the channel's audio
    def remove(self, event):
        self.channel.tracker.remove_channel(self.channel)

    def removeAll(self):
        for channelname in self.mother.channel_labels:
            self.mother.channelSettingsDict[channelname].pack_forget()
            
    def addAll(self):
        l = []
        for channelname in self.mother.channel_labels:
            l.append((self.mother.channelSettingsDict[channelname], self.mother.channel_labels[channelname][1]))

        l.sort(cmp=self.bySecondElement)
        
        for chSettings, idx in l:
            chS = ChannelSettings(chSettings.mother, chSettings.master, chSettings.channel)
            chS.pack(fill=BOTH, expand=1, side=TOP)
            self.mother.channelSettingsDict[chSettings.channel.name] = chS

    def bySecondElement(self, a, b):

        if a[1] > b[1]:
            return 1
        return -1

    def moveDown(self, event):
        if self.mother.channel_labels[self.name][1] == len(self.mother.channel_labels)-1:
            return

        self.removeAll()
        self.mother.tracker_pane.removeAll()

        # change order
        ch, idx = self.mother.channel_labels[self.name]
        idx += 1
        self.mother.channel_labels[self.name] = (ch, idx)
        for channelname in self.mother.channel_labels:
            a, b = self.mother.channel_labels[channelname]
            if b == idx:
                ch, idx = self.mother.channel_labels[channelname]
                idx -= 1
                self.mother.channel_labels[channelname] = (ch, idx)


        # readd all
        self.addAll()
        self.mother.tracker_pane.addAll()

    def moveUp(self, event):
        if self.mother.channel_labels[self.name][1] == 0:
            return
        
        self.removeAll()
        self.mother.tracker_pane.removeAll()

        # change order
        ch, idx = self.mother.channel_labels[self.name]
        idx -= 1
        self.mother.channel_labels[self.name] = (ch, idx)
        for channelname in self.mother.channel_labels:
            a, b = self.mother.channel_labels[channelname]
            if b == idx:
                ch, idx = self.mother.channel_labels[channelname]
                idx += 1
                self.mother.channel_labels[channelname] = (ch, idx)

        # readd all
        self.addAll()
        self.mother.tracker_pane.addAll()

class TrackerPane(Tkinter.Frame):
    """
    A TrackerPane is the graphical representation of a Tracker (see TEDmodel.py). 
    It's basically a stack of ChannelPanes with a Cursor ontop and a scrollbar
    below.
    And two more cursors, that mark the beginning and the end of a user defined loop!
    """
    
    def __init__(self, mother, master, controller, **opts):
        Tkinter.Frame.__init__(self, master, **opts)
        self.controller = controller
        self.scale_factor = 1.0/20
        # position of the red line, i.e. current dialogue time
        self.offset = 0
        self.cursor_relx = 0.8

        self.userSetArea = False
        self.userSetAreaStart = None
        self.userSetAreaEnd = None

        self.scale_factor_tkvar = Tkinter.IntVar()
        self.scale_factor_tkvar.set(int(1/self.scale_factor))
        self.tracker = controller.tracker
        self.cursor = Cursor(self)
                
        self.cursor.place(relx=self.cursor_relx, y=0, width=2, relheight=1)
        self.cursor.bind("<1>", self.cursor.dnd_start)

        self.channels = {}
        self.mother = mother

        self.bind("<Configure>", self.canvas_resized)
        self.width = None
        self.player_playing = False
        self.speed = 1.0
        self.rate = 25
        # Register as observer:
        self.tracker.register_for_channels(self.handle_channel_change)
        self.tracker.register_for_new_events(self.handle_new_event)
        # Player observers:
        self.player_observers = []
    
    def coordinateToTime(self, x):
        currentTime = self.offset
        currentScaling = self.scale_factor_tkvar.get()
            
        timeAtZeroCoord = currentTime - (self.width*currentScaling)/2.0
        return timeAtZeroCoord + x * currentScaling

    def timeToCoordinate(self, t):
        """
        Converts a time to the x Value of a coordinate.
        Returns -2 if x is out of the left bound of the screen. -1 is returned if out of right  bound.
        """
        
        t = float(t)
        currentTime = self.offset
        currentScaling = self.scale_factor_tkvar.get()
        timeAtZeroCoord = currentTime - (self.width*currentScaling)/2.0

        # Coordinate would be out of the left bound of the screen
        if t < timeAtZeroCoord:
            return -2

        result = (t - timeAtZeroCoord)/currentScaling
        if result < self.width:
            return result
        # Coordinate would be out of the right bound of the screen
        else:
            return -1
        
        
    
    def handle_new_event(self, event):
        self.update_scrollbar()
        
    def register_for_player_actions(self, observer):
        self.player_observers.append(observer)
        
    def unregister_for_player_actions(self, observer):
        self.player_observers.remove(observer)
        
    def notify_player_observers(self, action):
        for observer in self.player_observers:
            observer(action)
            
    def canvas_resized(self, event):
        """ Tells the TrackerPane that the canvas has been resized.  Triggers
        computation of new layout. """
        if self.width != None:
            dx = (event.width - self.width)*2*self.cursor_relx
            dt = dx / self.scale_factor
            self._move_to(self.offset - dt/2.0)
            self.update_scrollbar()
        self.width = event.width
        
    def xscrollcommand(self, callback):
        self.xscroll_callback = callback
        self.update_scrollbar()
        
    def update_scrollbar(self):
        if self.tracker.get_duration() == 0:
            x = 0.0
            y = 1.0
        else:
            page = self.winfo_width() / self.scale_factor
            start = -page/2.0
            end = self.tracker.get_duration() + page/2.0
            x = self.offset / (end-start)
            y = x + (page/(end-start))
        self.xscroll_callback(x, y)
        
    def xview(self, action, value, unit=None):
        value = float(value)
        #if action == 'scroll' and unit == PAGES  unit.lower()[:4] == "page":
        # Fixes some incompatibility on OSX, where Tkinter uses "PAGES" but Tk
        # "page".
        if action == 'scroll' and unit.lower()[:4] == "page":
            page = self.winfo_width() / self.scale_factor
            value *= page
            value = self.offset + value
            value = max(0, min(value, self.tracker.get_duration()))
            self.center_to(value)
        elif action=='moveto':
            value = max(0.0, min(1.0, value))
            page = self.winfo_width() / self.scale_factor
            value = (self.tracker.get_duration()+page) * value
            value = max(0, min(value, self.tracker.get_duration()))
            self.center_to(value)
        #elif action=='scroll' and unit==UNITS:
        elif action=='scroll' and unit.lower()[:4] == "unit":
            value *= 50 / self.scale_factor
            value = self.offset + value
            value = max(0, min(value, self.tracker.get_duration()))
            self.center_to(value)
        else:
            print action, value, unit
        # When skipping, we must restart the audio players:
        tkSnack.audio.stop()
        for pane in self.get_channel_panes():
            pane.currently_playing = []
        self.notify_player_observers("skip")
        
    def add_ruler(self):
        self.ruler = Ruler(self, height=22)
        self.ruler.pack(fill=X, expand=0)
        self.cursor.lift()

    def bySecondElement(self, a, b):
        if a[1] > b[1]:
            return 1
        return -1
    
    def handle_channel_change(self, action, channel):
        if action=="add":
            self.new_channel_pane(channel)
        elif action=="clear":
            self.clear_channel_pane(channel)
        elif action=="remove":
            self.remove_channel_pane(channel)

    def removeAll(self):
        for channel in self.channels:
            self.channels[channel].pack_forget()
            
    def addAll(self):
        l = []
        for channel in self.channels:
            for channelname in self.mother.channel_labels:
                if self.mother.channel_labels[channelname][0] == channel:
                    l.append((channel, self.mother.channel_labels[channelname][1]))
                else:
                    pass

        l.sort(cmp=self.bySecondElement)

        for ch, idx in l:
            for channel in self.channels:
                if channel == ch:
                    self.channels[channel].pack(fill=BOTH, expand=1)
            
    def new_channel_pane(self, channel):
        """ Creates a new ChannelPane with the specified name.  Label is the
        label widget associated with this ChannelPane.  The ChannelPane keeps a
        reference to this label in order to destroy it when the ChannelPane is
        destroyed.  """
        channel_pane = ChannelPane(self, channel, height=45,
                                   background="white")
        self.channels[channel] = channel_pane
        channel_pane.pack(fill=BOTH, expand=1)
        self.cursor.lift()

        self.update_scrollbar()
        return channel_pane
    
    def get_channel_panes(self):
        """ Returns a list of all ChannelPanes contained by a TrackerPane. """
        return [i for i in self.children.values() if i.__class__ == ChannelPane]
    
    def get_channel_pane(self, name):
        """ Returns the specified ChannelPane. /name/ is the name of the
        corresponding Channel. """
        return [i for i in self.get_channel_panes() if i.channel.name == name][0]
    
    def clear_channel_pane(self, channel):
        """ Clears all Events in the specified ChannelPane. """
        channel_pane = self.get_channel_pane(channel.name)
        channel_pane.clear()
        self.update_scrollbar()
        # TODO move current time into the new interval
        
    def remove_channel_pane(self, channel):
        """ First clears alle Events from the specified ChannelPane and then
        removes the ChannelPane itself. """
        channel_pane = self.get_channel_pane(channel.name)
        channel_pane.clear()
        channel_pane.destroy()
        self.update_scrollbar()
        
    def zoom(self, factor):
        """ Zooms the graphical representation by the given factor. """
        self.scale_factor *= factor
        for channel_pane in self.get_channel_panes():
            channel_pane.zoom(factor)
        self.ruler.zoom(factor)
        self.update_scrollbar()
        self.scale_factor_tkvar.set(int(1/self.scale_factor))
        
    def center_to(self, new_time):
        """Set new current dialogue time.  The dialogue will be shifted so that
        the new time is under the cursor line."""
        self._move_to(new_time)
        self.offset = new_time
        self.tracker.set_current_time(new_time)
        self.update_scrollbar()
        
    def _move_to(self, new_time):
        for channel_pane in self.get_channel_panes():
            channel_pane.center_to(new_time)
        self.ruler.center_to(new_time)
        
    def shift_to(self, dx):
        """Just slides the dialogue visually.  Doesn't change current time."""
        for channel_pane in self.get_channel_panes():
            channel_pane.shift_to(dx)
        self.ruler.shift_to(dx)
        
    def set_cursor(self, relx):
        self.cursor.place(relx=relx)
        self.cursor_relx = relx
        
    def dnd_accept(self, source, event):
        return source.__class__ == Cursor and self
    
    def dnd_enter(self, source, event):
        self.dnd_last_delta = 0
        self.dnd_origin = self.cursor_relx * self.winfo_width()
        
    def dnd_motion(self, source, event):
        x = self.winfo_pointerxy()[0] - self.winfo_rootx()
        x = float(x)
        # first, set the cursor
        relx = x / self.winfo_width()
        self.set_cursor(relx)
        # second, move the dialogue accordingly
        x -= self.dnd_origin
        dx = x - self.dnd_last_delta
        self.shift_to(dx)
        self.dnd_last_delta += dx
        
    def dnd_leave(self, source, event):
        pass
    
    def dnd_commit(self, source, event):
        self.dnd_last_delta = 0
        
    # Player stuff comes here.  Has to be done here because Tkinter doesn't
    # like strangers (other threads) to touch its internals lewdly.  Instead of
    # a separate thread we use TKinter's mechanism for scheduling events.
    # FIXME: Have a separate player instance.
    def play(self, startTime=None, endTime=None):
        
        if not endTime:
            endTime = self.tracker.get_duration()

        if not startTime:
            startTime = self.offset
            
        if self.player_playing:
            return
        self.player_playing = True
        self.player_last_time = None


        if self.userSetArea and not startTime and not endTime:
            pass
        else:
            self.playingStartTime = startTime
            self.playingEndTime = endTime

        self.notify_player_observers("play")
        if self.playingStartTime:
            self.center_to(self.playingStartTime)
        self._play()
        
    def _play(self):
        tracker = self.tracker

        startTime = self.playingStartTime
        endTime = self.playingEndTime
        
        if not self.player_last_time:
            self.player_last_time = time.time()*1000
        if not self.player_playing:
            return

        startTime = self.playingStartTime
        endTime = self.playingEndTime
        
        new_time = time.time()*1000
        delta = new_time - self.player_last_time
        delta *= self.speed
        new_current_time = self.offset + delta

        if new_current_time > endTime:
            # We reached the end of the dialogue.
            # Go to beginning and keep playing
            #if startTime:
            if self.userSetAreaStart:
                #print "DEBUG: range was set! replaying from " + str(startTime)
                # stop current audio playback. else your sound will keep playing to the end, although the loop ended before.
                tkSnack.audio.stop()
                self.notify_player_observers("stop")
                self.notify_player_observers("play")
                
                self.center_to(startTime)
                self.update_idletasks()
                self.after(self.rate, self._play)
            else:
                # ..?
                #print "DEBUG: no range set, replaying from 0"
                self.center_to(self.tracker.get_duration())
                self.controller.toggle_play()
                
        else:
            #print "DEBUG: end not reached yet. new time is " + str(new_current_time)
            self.center_to(new_current_time)
            self.player_last_time = new_time
            self.update_idletasks()
            #print "DEBUG: start n endtime of self: " + str(self.playingStartTime), str(self.playingEndTime)
            self.after(self.rate, self._play)
            
    def stop(self):
        self.player_playing = False
        self.player_last_time = None
        tkSnack.audio.stop()
        # das, 050908: ok. So the problem was that things weren't taken
        #   away from the currently_playing list. They were automatically
        #   removed *after* their scheduled playing time, but for long
        #   audio events (actually, even for moderately sized ones) this will
        #   be way too late and play will have been hit again. So
        #   we go through all panes and empty the lists by brute force here..
        for pane in self.get_channel_panes():
            pane.currently_playing = []
        self.notify_player_observers("stop")
        
    def accelerate(self, factor):
        self.speed *= factor

class Cursor(Tkinter.Label):
    def __init__(self, master, **opts):
        opts['text'] = ""
        opts['bg'] = "red"
        Tkinter.Label.__init__(self, master, **opts)
        self.offset = 0
    def dnd_start(self, event):
        Tkdnd.dnd_start(self, event)
    def dnd_end(self, target, event):
        pass

class Ruler(Tkinter.Canvas):
    # This guy basically works like channel panes do, i.e. it has the same
    # rendering strategy and zoom and pan logic.
    # TODO: Allow drag&drop on the ruler to pan the dialogue.  Alternatively:
    # implement it for tracker_pane.
    def __init__(self, master, **opts):
        Tkinter.Canvas.__init__(self, master, **opts)
        self.tracker_pane = master

        #self.bind_all('<Button-1>', self.clicked)
        self.bind('<Button-1>', self.clicked)
        self.bind('<Button-3>', self.__rightClickMenu__)
        self.bind('<Button-2>', self.__rightClickMenu__)

        self.ids = []
        # timespan where ticks are already drawn:
        self.start = 0.0
        self.end = 0.0

    def __void__(self):
        pass

    def __rightClickMenu__(self, event):
        xTime = self.tracker_pane.coordinateToTime(event.x)

        self.menu = Tkinter.Menu(self.tracker_pane, tearoff=0)

        self.menu.add_command(label="Add bookmark", command=lambda:self.tracker_pane.controller.setBookmark(xTime))
        self.menu.add_command(label="Set Loop start here", command=self.__void__)
        self.menu.add_command(label="Set Loop end here", command=self.__void__)
        
        try:
            self.menu.post(event.x_root, event.y_root)
        except:
            print "Error. Something went wrong while trying to post the menu."
            pass
        
    def clicked(self, event):
        #offset = self.tracker_pane.offset
        #scale_factor = self.tracker_pane.scale_factor
        #cursor_x = self.winfo_width() * self.tracker_pane.cursor_relx
        #click_t = offset + ((event.x - cursor_x) / scale_factor)
        #click_t = max(0, click_t)
        #click_t = min(self.tracker_pane.tracker.end, click_t)
        click_t = self.tracker_pane.coordinateToTime(event.x)

        self.tracker_pane.center_to(click_t)
        
    def update(self):
        """ Checks if the length of the dialogue has changed (increased).  If yes,
        new ticks are drawn as needed.  TrakerPane calls this method when new
        events are added. """
        # There are as many ticks as there are seconds in the dialogue.  I
        # guess this implementation is not capable of handling ticks for the
        # milliseconds as this would amount to 30*60*1000 objects that have to
        # be scaled and moved in a 30min dialogue.
        tracker = self.tracker_pane.tracker
        if tracker.end > self.end:
            start = int(math.ceil(self.end))
            end = int(math.floor(tracker.end))
            for t in range(start, end, 1000):
                scale_factor = self.tracker_pane.scale_factor
                offset = self.tracker_pane.offset
                clipping_offset = offset - (self.winfo_width()
                                         * self.tracker_pane.cursor_relx) / scale_factor
                object_offset = (t - clipping_offset) * scale_factor
                if (t/1000) % 60 == 0:
                    self.create_line(object_offset, 0, object_offset, 9, tag="minutes")
                elif (t/1000) % 10 == 0:
                    self.create_line(object_offset, 0, object_offset, 6, tag="ten_seconds")
                else:
                    self.create_line(object_offset, 0, object_offset, 3, tag="seconds")
                id = self.create_text((object_offset, 16), text=tools.ms_to_time_short(t))
                self.ids.append(id)
            self.end = tracker.end
            
    def zoom(self, factor):
        # hide/show second ticks
        if self.tracker_pane.scale_factor == 0.0015625:
            self.itemconfig("seconds", state=HIDDEN)
        elif self.tracker_pane.scale_factor == 0.003125:
            self.itemconfig("seconds", state=NORMAL)

        width = self.winfo_width()
        step_size = 0.1 / self.tracker_pane.scale_factor
        for idx, id in enumerate(self.ids):
            if idx % step_size:
                self.itemconfig(id, state=HIDDEN)
            else:
                self.itemconfig(id, state=NORMAL)
        self.scale(ALL, width*self.tracker_pane.cursor_relx, 1, factor, 1)
        
    def center_to(self, time):
        dt = self.tracker_pane.offset - time
        dx = dt * self.tracker_pane.scale_factor
        self.move(ALL, dx, 0)
        
    def shift_to(self, dx):
        self.move(ALL, dx, 0)

class ProtoChannelPane(Tkinter.Canvas):
    """ mother of all types of channel panes that all contain a Channel (sound, alignment, tier, you name it, see TEDmodel->Channel) """
    def __init__(self, master, channel, **opts):
        Tkinter.Canvas.__init__(self, master, **opts)
        #print "jippie, i'm a protochannelpane!"
        self.tracker_pane = master
        self.channel = channel
        self.events = {} # dictionary, but what's used as the key?
        self.bind('<Button-1>', self.clicked)
        
        # For observers:
        channel.register_for_latest_events(self.handle_latest_events)
        channel.register_for_latest_events(self.handle_latest_events_audio)
        self.channel.register_for_new_events(self.handle_new_event)
        
    def clicked(self, event):
        # placeholder for the class to compile. is overwritten by
        # inheriting classes.
        pass
    
    def remove(self, event):
        self.tracker_pane.tracker.remove_channel(self.channel)
        
    def show_event(self, event):
        pass # abstract method
    
    def hide_event(self, event):
        pass # abstract method
    
    def highlight_event(self, event):
        pass # abstract method
    
    def show_subevents(self, event):
        for subevent in event.subevents:
            self.show_event(subevent)
            
    def hide_subevents(self, event):
        for subevent in event.subevents:
            self.hide_event(subevent)
            
    def highlight_subevents(self, event):
        for subevent in event.subevents:
            self.highlight_event(subevent)
            
    def handle_latest_events(self, latest_events):
        """ Show subevents of most recent events and hide all other subevents. """
        # Collect all first-order events (i.e. non-subevents) ...
        events = filter(lambda e:not e.subevent, self.events.keys())
        # ... and calculate the complement of latest_events:
        for event in latest_events:
            events.remove(event)
        # Show subevents of latest events:
        for event in latest_events:
            self.show_subevents(event)
        # Hide subevents of all other events:
        for event in events:
            self.hide_subevents(event)
            
    def handle_player_action(self, action): # FIXME: this looks like audio functionality
        latest_events = self.channel.latest_events()
        if action == "play":
            self.handle_latest_events_audio(latest_events)
        elif action == "skip":
            self.handle_latest_events_audio(latest_events)
            
    def handle_latest_events_audio(self, latest_events): # FIXME: this one even more
        pass # abstract method
    
    def handle_new_event(self, event):
        self.add_event(event)
        
    def add_subevent(self, event):
        id = self.add_event(event)
        self.itemconfig(id, state=HIDDEN)
        
    def unfold_widget(self, event):
        pass # abstract method
    
    def fold_widget(self, event):
        pass # abstract method
    
    def add_event(self, event):
        pass # abstract method
    
    def zoom(self, factor):
        pass # abstract method
    
    def clear(self):
        self.delete(ALL)
        
    def center_to(self, time):
        dt = self.tracker_pane.offset - time
        dx = dt * self.tracker_pane.scale_factor
        self.move(ALL, dx, 0)
        
    def shift_to(self, dx):
        self.move(ALL, dx, 0)

class ChannelPane(ProtoChannelPane):
    """ ChannelPane contains one Channel (sound, alignment, tier, you name it, see TEDmodel->Channel) """
    def __init__(self, master, channel, **opts):
        ProtoChannelPane.__init__(self, master, channel, **opts)
        self.ids = {} # seems to be alignment-sequence-specific
        self.currently_playing = [] # media, obviously media-specific
        self.tracker_pane.register_for_player_actions(self.handle_player_action) # media-specific?

        
    def clicked(self, event):
        for id in self.find_closest(event.x, event.y):
            event = self.ids[id]
            self.highlight_subevents(event)
            if event.sound:
                event.sound.play()
            
    def show_event(self, event):
        id = self.events[event]
        self.itemconfig(id, state=NORMAL)
        self.tracker_pane.update_scrollbar()
        
    def hide_event(self, event):
        id = self.events[event]
        self.itemconfig(id, state=HIDDEN)
        self.tracker_pane.update_scrollbar()
        
    def highlight_event(self, event):
        if self.itemcget(self.events[event], 'state') == NORMAL:
            return
        self.show_event(event)
        # FIXME use self.after!
        thread.start_new_thread(lambda:time.sleep(3) or self.hide_event(event), ())
        
    def handle_latest_events_audio(self, latest_events):
        if (not self.tracker_pane.player_playing
            or self.tracker_pane.speed != 1.0):
            return
        current_time = self.tracker_pane.offset
        latest_sound_events = filter(lambda e:e.sound, latest_events)
        for event in latest_sound_events:
            if (not event in self.currently_playing
                and current_time < event.time+event.sound.duration):
                offset = int((current_time - event.time)
                             * event.sound.frequency / 1000)
                try:
                    event.sound.play(start=offset)
                except Tkinter.TclError, e:
                    tools.log(e)
                self.currently_playing.append(event)
                # NOTE: Apparently the callback on play doesn't work due to a
                # bug in TkSnack or the Python bindings to TkSnack.  Therefore
                # we schedule our own callback using after.  This will raise an
                # exception if the player was stopped in the meantime but it
                # doesn't do any harm.  Well, except if in the meantime the
                # player was stopped and started again.  FIXME
                self.after(int(math.ceil(event.sound.duration)+1),
                           lambda:self.currently_playing.remove(event))
    def unfold_widget(self, event):
        """ Expands a bar widget as much as needed in order to make the whole
        text readable. """
        # first event: GUI event handling, second event: dialog object
        id = self.events[event.widget.event]
        scale_factor = self.tracker_pane.scale_factor
        if event.widget.winfo_reqwidth() > event.widget.event.duration * scale_factor:
            self.itemconfig(id, width=event.widget.winfo_reqwidth())
            event.widget.lift()
    def fold_widget(self, event):
        """ Undo unfolding, i.e. scale a bar back to the adequate size given the
        scale factor. """
        id = self.events[event.widget.event]
        scale_factor = self.tracker_pane.scale_factor
        self.itemconfig(id, width=event.widget.event.duration * scale_factor)
    def add_event(self, event):
        global height_label
        scale_factor = self.tracker_pane.scale_factor
        offset = self.tracker_pane.offset
        clipping_offset = offset - (self.winfo_width()
                                 * self.tracker_pane.cursor_relx) / scale_factor
        object_offset = (event.time - clipping_offset) * scale_factor
        if event.duration > 0 and not event.sound: # this is an ordinary label
            width=event.duration * scale_factor
            # TODO: Enforce some minimal wraplength.
            widget = Tkinter.Label(self, text=event.__str__(),
                background=event.color, anchor=NW, justify=LEFT, relief=RAISED,
                bd=1, padx=2, font=("Helvetica", 9))
            widget.event = event
            id = self.create_window(
                object_offset, 24,
                width=event.duration * scale_factor,
                height=height_label,
                anchor=NW,
                window=widget)
            widget.bind('<Enter>', self.unfold_widget)
            widget.bind('<Leave>', self.fold_widget)
        elif event.duration > 0 and event.sound: # this is a sound label
            #print "sound!!"
            # das, 080708: am trying to replace snack widgets with
            #   normal widgets, doesn't seem to work yet.
#            widget = tkSnack.SnackCanvas(self, height=20,
#                                         width=event.duration*scale_factor)
            widget = Tkinter.Label(self, text="sound",
                background=event.color, anchor=NW, justify=LEFT, relief=RAISED,
                bd=1, padx=2)
            event.sound.widget = widget
            id = self.create_window(
                object_offset, 24,
                width=event.duration * scale_factor,
                height=height_label,
                anchor=NW,
                window=widget)
#            event.sound.render(scale_factor)
#            id = self.create_window(
#                object_offset, 2,
#                width=event.duration * scale_factor,
#                height=42,
#                anchor=NW,
#                window=widget)
        else: # this event contains subevents -> render as diamond
            if (event.isPoint):
                x = object_offset
                y = int((1 - event.height) * self.winfo_height())
                dia = 3
                id = self.create_oval((x, y, x + dia, y + dia), fill=event.color, outline=event.outline_color, tags="point")
            else:
                polygon = (6, 1, 11, 11, 6, 21, 1, 11)
                polygon_offset = (-6, 0) * 4
                object_offset = (object_offset, 0) * 4
                polygon = map(lambda x, y, z: x + y + z,
                              polygon, polygon_offset, object_offset)
                id = self.create_polygon(polygon, fill=event.color, outline=event.outline_color,
                                         tags="point")
        for subevent in event.subevents:
            self.add_subevent(subevent)
        self.ids[id] = event
        self.events[event] = id
        return id
    def zoom(self, factor):
        """ scale all items shown to the correct zooming factor """
        # FIXME: Change wraplength of labels to new width.
        # NOTE: If labels are self.scaled only, they get screwed up when first
        # zooming out and then in again.  The problem are 1. that coordinates
        # are rounded to the next int and 2. that labels decide to go to their
        # preferred size when told to have only a fraction of a pixel as their
        # width.  Solution: itemconfig them to the correct size.
        width = self.winfo_width()
        scale_factor = self.tracker_pane.scale_factor
        # This is only used for moving the objects to the right place:
        # scale() is a Tkinter.Canvas function
        self.scale(ALL, width*self.tracker_pane.cursor_relx, 1, factor, 1)
        for id, event in self.ids.items():
            if event.duration == 0 and not event.sound:
                if not event.isPoint:
                    # correction of the skewed rhombs:
                    _, _, r, _, _, _, l, _ = self.coords(id)                    
                else:
                    # correction for points:
                    r, _, l, _ = self.coords(id)
                    #self.scale(id, factor, 1, x1, 1)
                self.scale(id, (l+r)/2.0, 1, 1.0/factor, 1)
            # das, 040908: switched off special canvases for sound:
            #elif event.sound:
            #    self.itemconfig(id, width=event.duration * scale_factor)
            #    event.sound.render(scale_factor)
            else:
                # correct the size of labels (screwed up by rounding to whole
                # pixels):
                # das, 080708: trying to adjust wraplength to new
                #   zoom factor, but tk tells me that id doesn't take
                #   option "wraplength", even though that's a standard option!
                #self.itemconfig(id, wraplength=int(event.duration * scale_factor))
                #print self.itemconfig(id)
                #self.scale(id, width*self.tracker_pane.cursor_relx, 1, factor, 1)
                self.itemconfig(id, width=event.duration * scale_factor)

class AudioPlayer(object):
    def __init__(self, tracker):
        # We need to know about channels.
        tracker.register_for_channels(self.handle_channel_change)
        tracker.register_for_player_actions(self.handle_player_action)
    def handle_channel_change(self, action, channel):
        if action=="add":
            channel.register_for_latest_events(self.handle_latest_events)
    def handle_player_action(self, action):
        pass
    def handle_latest_events(self, latest_events):
        pass



