
"""Various auxiliary functions used by TEDView."""

import sys

class GraphNodes:

    def __init__(self):
        self.nodeMapInt = {}

    def get(self, ident):
        if type(ident) == int:
            try:
                return self.nodeMapInt[ident]
            except KeyError:
                return None

    def add(self, key, val):

        self.nodeMapInt[key] = val

    def reset(self):
        self.nodeMapInt = {}

        

def log(message):
    """Prints the string represenatation of the given object to stderr.  Used
    for printing debug messages."""
    print >> sys.stderr, "%s" % message

def ms_to_time(milliseconds):
    """Converts milliseconds in HH:MM:SS:MS (MS). """
    milliseconds = milliseconds2 = int(milliseconds)
    hours = milliseconds / 3600000
    milliseconds -= hours * 3600000
    minutes = milliseconds / 60000
    milliseconds -= minutes * 60000
    seconds = milliseconds / 1000
    milliseconds -= seconds  * 1000
    return "%02d:%02d:%02d.%03d (%s)" % (hours, minutes, seconds, milliseconds,
                                         milliseconds2)

def ms_to_time_short(milliseconds):
    """Converts milliseconds in MM:SS. """
    milliseconds = milliseconds2 = int(milliseconds)
    hours = milliseconds / 3600000
    milliseconds -= hours * 3600000
    minutes = milliseconds / 60000
    milliseconds -= minutes * 60000
    seconds = milliseconds / 1000
    return "%02d:%02d" % (minutes, seconds)

def list_union(lists):
    """Returns the concatenation of the lists in lists.  Order of elements is
    preserved. """
    res = []
    for l in lists:
        res.extend(l)
    return res

#
# Convenience functions for processing XML.
#

def all_child_nodes(node):
    """Returns a list of all childNodes of 'node' and their childNodes
    recursively."""
    child_nodes = node.childNodes
    for child_node in node.childNodes:
        child_nodes.extend(all_child_nodes(child_node))
    return child_nodes

def all_leaf_nodes(node):
    """Returns a list of all subordinate nodes of 'node' that are leaf nodes,
    i.e. have no childNodes themselfes.  If 'node' itself is a leaf, the
    returned list will contain only 'node'."""
    if not node.childNodes:
        return [node]
    else:
        leaf_nodes = [all_leaf_nodes(n) for n in node.childNodes]
        leaf_nodes = reduce(lambda l1, l2:l1+l2, leaf_nodes, [])
        return leaf_nodes

def child_nodes_by_name(node, listOfNames):
    """Returns a list of all direct childNodes that have the name 'name'."""
    if type(listOfNames) == str:
        listOfNames = [listOfNames]

    # n.nodeType == 1 seems to be a node
    # n.nodeType == 3 is TEXT, iff <node someattrs=someval>TEXT</node>
    return [n for n in node.childNodes if n.nodeType == 1 and n.tagName in listOfNames]


def get_attribute_default(node, attribute_name, default_value=AttributeError):
    """Returns the value 'attribute_name' of 'node' if defined, otherwise
    return default_value.  If no default value is specified and the respective
    attribute is not defined, an AttributeError will be raised.
    (Node.getAttribute returns empty strings as the value of undefined
    attributes.)"""
    if node.hasAttribute(attribute_name):
        return node.getAttribute(attribute_name)
    elif default_value == AttributeError:
        raise AttributeError("Attribute '%s' not found." % attribute_name)
    else:
        return default_value

def get_content(node):
    """Returns the content of the given node as a string."""
    result = u''
    for n in node.childNodes:
        if len(n.childNodes) == 0:
            result += n.toxml()
        else:
            result += get_content(n)
    return result
    
    
