#!/usr/bin/perl -w

########################################################################
#
# file:   start_ted.perl
# date:   09/09/08
# author: das
#
# Wrapper around TEDview and TEDqtplayer. Starts both, with correct
#  paths, and correct offsets.


use strict;


#my $dialogue = "20061117_run2pento_noise";
my $dialogue = shift;

#my $ted_base = "/Users/das/uni_sync/work/projects/Emmy/Software/cvsd/TEDview";
my $ted_base = "TEDpython";
my $tedview = $ted_base . "/datasink.py";
my $tedsource = $ted_base . "/datasource.py";

my $tedqt = "./TEDqtplayer.jar";

my $movie_base = "AlternativeVideos";
my $this_movie = $movie_base . "/" . $dialogue . ".mov";

#my $tad_base = "~/Projects/Corpora/Own_Corpora/DEAWU/Noise/Final/TADs";
my $tad_base = "TAD";
my $this_tad = $tad_base . "/wsound_" . $dialogue . "_zw.xml";

# offsets between video and audio. Hardcoded.
# Taken from Software/PreparingData/Mouse2csv/RawData/*params.txt
# Calculated as "audio start - (frame / frame rate)".
my %offsets = (
    "20061117_run2pento_noise" => 53.72655518,
#    "20061123_pento_nonoise" => 77.07313894,
    "20070117_run1pento_noise" => 36.5887062,
    "20070131_run1pento_noise" => 4.18, #4.290896047,
    "20070131_run2pento_noise" => 5.05, #5.23670035,
#    "20070131_run3pento_noise" => 5.23670035,
    "20070201_run1pento_nonoise" => 10.694445975022,
    "20070201_run3pento_nonoise" => 3.745208122479,
    "20070201_run4pento_nonoise" => 4.1 #4.476847587069
    );


my $this_offset = $offsets{$dialogue} or die "unknown dialogue!";


my $command1 = "java -jar $tedqt $this_movie &";
print $command1 . "\n";
system($command1);
sleep 3;

my $command2 = "($tedview -o $this_offset | nc localhost 9123) &";
print $command2 . "\n";
system($command2);
sleep 3;

my $command3 = $tedsource . " " . $this_tad;
print $command3 . "\n";
system($command3);


