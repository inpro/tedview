import Tkinter
# see the file COPYING for license details!
import xml.dom.minidom

from nltk.tree import Tree
from nltk.draw.util import CanvasFrame
from nltk.draw.tree import TreeWidget
from PyGraph.tkgraphview import *
import auxiliary_tools as tools

class InspectorFrame(Tkinter.Toplevel):
    """Plug-ins for the various content-types subclass this class."""
    def __init__(self, master, tracker, **opts):
        Tkinter.Toplevel.__init__(self, master, **opts)
        self.tracker = tracker

        self.__plugins__ = {
            "syntax": SyntaxInspector,
            "dist": DistributionInspector,
            "Syntax": SyntaxInspector,
            "Distribution": DistributionInspector,
            "default": DefaultInspector,
            "XML": XMLInspector,
            "SLL+GIL": SSL_GI_Inspector
        }

        
        # Channel and Inspector selector:
        self.channelSelectorPane = Tkinter.Frame(self)
        self.inspectorSelectorPane = Tkinter.Frame(self)
        self.selected_channel_var = Tkinter.StringVar(master)
        self.selectedInspectorType = Tkinter.StringVar(master)
        self.selected_channel_var.set("None")
        self.selectedInspectorType.set("None")

        inspectorNames = [ "SLL+GIL", "Syntax", "Distribution", "XML", "default" ]
        Tkinter.Label(self.inspectorSelectorPane, text="Inspector:").pack(side=Tkinter.LEFT)
        
        channel_names = [ch.name for ch in tracker.get_channels()]
        Tkinter.Label(self.channelSelectorPane, text="Channel:").pack(side=Tkinter.LEFT)
        
        self.channel_selector = Tkinter.OptionMenu(self.channelSelectorPane, self.selected_channel_var, *channel_names)
        self.channel_selector.pack(side=Tkinter.LEFT)

        self.inspectorSelector = Tkinter.OptionMenu(self.inspectorSelectorPane, self.selectedInspectorType, *inspectorNames)
        self.inspectorSelector.pack(side=Tkinter.LEFT)
        
        self.tracker.register_for_channels(self.handle_channel_change)
        
        self.selectorOK = Tkinter.Button(self.channelSelectorPane, text="OK",command=self.selectChannel)
        self.inspectorOK = Tkinter.Button(self.inspectorSelectorPane, text="Show", command=self.select)
        
        self.selectorOK.pack(side=Tkinter.RIGHT)
        self.channelSelectorPane.pack()
        # Info frame:
        self.inspector_widget = None

    def selectChannel(self):
        self.inspectorSelectorPane.pack()
        self.inspectorOK.pack(side=Tkinter.RIGHT)
    
    def select(self):
        channel = self.tracker.get_channel(self.selected_channel_var.get())
        
        if self.inspector_widget:
            self.inspector_widget.destroy()

        # for SLLs and GILs, set "channel" to tracker
        if self.selectedInspectorType.get() == 'SLL+GIL':
            channel = self.tracker
        
        inspectorWidget = self.__getPlugin__(self.selectedInspectorType.get())
        self.inspector_widget = inspectorWidget(self, channel)
        self.inspector_widget.pack(fill=Tkinter.BOTH, expand=1)
        
    def handle_channel_change(self, action, channel):
        menu = self.channel_selector['menu']
        menu.delete(0, Tkinter.END)
        menu.add_command(label="None",
                          command=lambda:self.selected_channel_var.set(""))
        for ch_name in [ch.name for ch in self.tracker.get_channels()]:
            menu.add_command(label=ch_name, command=lambda v=self.selected_channel_var,l=ch_name:v.set(l))
        if action=="remove" and channel.name==self.selected_channel_var.get():
            self.selected_channel_var.set("None")

    def __getPlugin__(self, contentType):
        return self.__plugins__.get(contentType, DefaultInspector)
            

class InspectorWidget(Tkinter.Frame):
    def __init__(self, master, channel):
        Tkinter.Frame.__init__(self, master)
        self.channel = channel
        channel.register_for_latest_events(self.handle_latest_events)
    def handle_latest_events(self, latest_events):
        self.display(latest_events)
    def destroy(self, *args, **opts):
        self.channel.unregister_for_latest_events(self.handle_latest_events)
        Tkinter.Frame.destroy(self, *args, **opts)

class InspectorWidgetForAllChannels(Tkinter.Frame):
    def __init__(self, master, tracker):
        Tkinter.Frame.__init__(self, master)
        self.tracker = tracker
        
    def handle_latest_events(self, latest_events):
        self.display(latest_events)
        
    def destroy(self, *args, **opts):
        for channel in self.tracker.get_channels():
            channel.unregister_for_latest_events(self.handle_latest_events)
        Tkinter.Frame.destroy(self, *args, **opts)

class SyntaxInspector(InspectorWidget):
    
    def __init__(self, master, channel, **opts):
        InspectorWidget.__init__(self, master, channel, **opts)
        self.channel = channel
        self.cf = CanvasFrame(parent=self)
        self.cf.pack()
        self.trees_on_display = []
        self.display(channel.latest_events())
        
    def display(self, events):
        while self.trees_on_display:
            tw = self.trees_on_display.pop()
            self.cf.destroy_widget(tw)
            
        for event in events:
            tree = Tree.parse(unicode(event))
            tw = TreeWidget(self.cf.canvas(), tree, draggable=1)
            self.cf.add_widget(tw, 10, 10)
            tw.bind_click_trees(tw.toggle_collapsed)
            self.trees_on_display.append(tw)


class DistributionInspector(InspectorWidget):
    def __init__(self, master, channel, **opts):
        InspectorWidget.__init__(self, master, channel, **opts)
        self.channel = channel
        self.cf = CanvasFrame(parent=self)
        self.cf.canvas().configure(width=300, height=150)
        self.cf.pack(fill='both', expand='yes')
        self.bars_on_display = []
        self.display(channel.latest_events())
        
    def display(self, events):
        canvas = self.cf.canvas()
        
        while (self.bars_on_display):
            bar = self.bars_on_display.pop()
            canvas.delete(bar)
            
        for event in events:
            # get the distribution from the event
            content = unicode(event)
            lines = content.split("\n")
            # prepare dimension variables
            maxX = int(canvas.cget('width'))
            maxY = int(canvas.cget('height'))
            numBars = len(lines)
            barWidth = float(maxX) / float(numBars)
            i = 0
            for line in lines:
                (key, value) = line.split(" ")
                value = float(value)
                bar = canvas.create_rectangle(i * barWidth, maxY, (i + 1) * barWidth, maxY * (1. - value), fill=event.color)
                self.bars_on_display.append(bar)
                label = canvas.create_text((i + 0.5) * barWidth, 20, text=key);
                self.bars_on_display.append(label)
                i = i + 1
                
class DefaultInspector(InspectorWidget):
    """Default Text Inspector used for unknown content-types.  Displays a string
    representation of the content."""
    def __init__(self, master, channel, **opts):
        InspectorWidget.__init__(self, master, channel, **opts)
        self.channel = channel
        self.content_var = Tkinter.StringVar("") 
        self.label = Tkinter.Label(self, textvariable=self.content_var, justify='left') 
        self.label.pack()
        self.display(channel.latest_events())
        
    def display(self, events):
        string = "\n".join(["%s" % e for e in events])
        self.content_var.set(string)
         
class XMLInspector(DefaultInspector):
    
    def display(self, events):
       string = "\n".join(["%s" % e.serialize_to_xml() for e in events])
       # TODO: this should really be pretty-printed!
       self.content_var.set(string)

class SSL_GI_Inspector(InspectorWidgetForAllChannels):
    def __init__(self, master, tracker, **opts):
        InspectorWidgetForAllChannels.__init__(self, master, tracker, **opts)
        self.widgetsOnDisplay = []
        
        self.tracker = tracker
        self.cf = CanvasFrame(parent=self)

        self.cf.pack(fill="both", expand="yes")
        self.lastDrawnNodes = tools.GraphNodes()
        self.i = 1
        self.nodesToDraw = []
        self.lastEvents = []
        latestTrackerEvents = []
        self.numberOfUpdates = 0

        self.tracker.registerForAllChannelsLatestEvents(self.display)
        """
        for ch in self.tracker.get_channels():
            ch.register_for_new_events(self.handle_latest_events)
        """
        
    def handle_latest_events(self, latest_events):
        self.display(latest_events)


    def display(self, events):
        """
        assume events is a dictionary of channel names containing lists of lists of events. the latest list in the list (i.e., dict[channelName][-1])
        is then the collection of events to be shown by the graph.
        this is true for each channel.

        then nodesToDraw can be set to the empty list every time and we can then loop over the union of dict[channelName][-1] for all channel names.
        """

        self.lastDrawnNodes = tools.GraphNodes()
        self.nodesToDraw = []
        latestEvents = []

        #for channelName in events:
        #    latestEvents.extend(events[channelName])
        latestEvents = events
        
        for event in latestEvents:

            subevents = []
            if event.diamond:
                subevents = event.subevents
            else:
                subevents.append[event]
            
            for sub in subevents:
                for se in sub.subevents:
                    
                    nodeName = str(se.iu_id)
                    nodeID = se.iu_id
                    
                    self.i += 1
                    
                    existingPaintedNode = self.lastDrawnNodes.get(nodeID)

                    currentNode = {}
                    if existingPaintedNode:
                        for paintedNode in self.nodesToDraw:
                            if paintedNode['node'] == nodeName:
                                if se.SLLs == None:
                                    continue
                                for link in se.SLLs.split(","):
                                    if link == "top":
                                        continue
                                    paintedNode['edges'].append(self.lastDrawnNodes.get(link))
                                    paintedNode['edgenames'].append("SLL")
                    else:

                        self.lastDrawnNodes.add(nodeID, nodeName)
                        currentNode['node'] = nodeName
                        currentNode['label'] = str(se)
                        currentNode['shape'] = 'rect'
                        currentNode['features'] = ''
                        currentNode['color'] = 'lightblue'
                        currentNode['edges'] = []
                        currentNode['edgenames'] =[]
                        currentNode['edgeColour'] = []
                        currentNode['yEquivClass'] = se.originator
                    
                        if se.SLLs:
                            # list of IDs the link leads to
                            #print "found same levelinks"
                            sameLevelLinks = se.SLLs
                            for link in sameLevelLinks.split(","):
                                if link == "top":
                                    # meaning of "top" is no SLL
                                    continue
                                currentNode['edges'].append(str(link))
                                #currentNode['edges'].append(self.lastDrawnNodes.get(link))
                                currentNode['edgenames'].append("SLL")
                            currentNode['color'] = 'lightblue'
                        if se.GILs:
                            # list of IDs the link leads to
                            groundedInLinks = se.GILs

                            # for every arc that is added here, the target node has to be added, bc its not the events of this channel.
                            # how to get the content of that node? is that even possible?
                            for link in groundedInLinks.split(","):
                                currentNode['edges'].append(str(link))
                                currentNode['edgenames'].append("GIL")

                        self.nodesToDraw.append(currentNode)

            """
            cw = tkGraphView(self.nodesToDraw, "Graph Viewer Title", root=self.cf.canvas(), outfile="myChart", graphtype='gridded',startTk=False)
            self.cf.add_widget(cw)
            self.widgetsOnDisplay.append(cw)
            """
            if self.numberOfUpdates == 0:
                self.cf = tkGraphView(self.nodesToDraw, "Graph Viewer Title", root=self.cf.canvas(), outfile="myChart", graphtype='gridded',startTk=False)
            if self.numberOfUpdates > 0:
                self.cf.removeAll()
                self.cf.addNodesFromArray(self.nodesToDraw)
                self.cf.draw()

            self.numberOfUpdates += 1;
            """
            try:
                self.cf.destroy_scrollbar()
                self.cf = tkGraphView(self.nodesToDraw, "Graph Viewer Title", root=self.cf.canvas, outfile="myChart", graphtype='gridded',startTk=False)
                print "1"
            except:
                self.cf = tkGraphView(self.nodesToDraw, "Graph Viewer Title", root=self.cf.canvas(), outfile="myChart", graphtype='gridded',startTk=False)
                print "2"
            """


    def addEdgeToNodeList(self, listOfNodes, fromNode, toNode, label, color="black"):
        result = []
        
        for cNode in listOfNodes:
            if cNode.get('node') == fromNode:
                if cNode.has_key('edges'):
                    cNode['edges'].append(toNode)
                    cNode['edgenames'].append(label)
                else:
                    cNode['edges'] = list(toNode)
                    cNode['edgenames'] = list(label)
            
            result.append(cNode)
                
        return result
