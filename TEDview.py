#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This is TEDView, a viewer for time-based data.  It was developed for
displaying the activities during processing of dialogues on- and offline along
with annotations and gold standards.  Events that TEDview can display comprise
point-events, spans which can be labeled, and audio snippets."""

# CHANGES:
# das, 050908:
#   basically two big changes:
#   - found out that drawing waveforms or spectrograms is actually what
#     causes the problems with large files. Managed to use normal label
#     widgets instead of the snack ones, and now large files load.
#     TO DO: this should actually be a parameter. That is, it should be left
#     to the creator of the datafile to specify the way the sound should
#     be displayed, as normal label, or as waveform or spectogram.
#     At the moment, snack widgets are completely disabled.
#   - there was a bug in how the audio file to play when restarting the
#     system was searcherd for, and also in how playing in general was
#     managed. This is fixed now (see comments below).
# das, 090908:
#   output for controlling video player, and python called in unbuffered
#   mode (-u flag)


# Pyhton stdlibs:
import sys, os
import Tkinter

# TEDview modules:
from TEDserver import *
from TEDgui import *

# das, 040908: am possibly perl-ing up the code here, but it think it's
#  useful to have some things available at least as global parameters
#  in the code:
# - the height of the label widgets. Used to be at 20, changed to 30 to
#   get more text (now line wrapped) to view.
#   tvdm: moved to TEDgui.py


def main():
    # setting defaults:
    ipaddress = '127.0.0.1'
    port = 2000
    vid_offset = -1
    filename = None
    vidPort = None

    # Processing command line arguments:
    argv = sys.argv[1:]
    while argv:
        arg = argv.pop(0)
        if arg == '-p':
            port = int(argv.pop(0))
        elif arg == '-i':
            ipaddress = argv.pop(0)
        elif arg == '-o':
            vid_offset = float(argv.pop(0))
	elif arg == '-vp':
	    vidPort = int(argv.pop(0))
        # TODO: offer help via -h (it's not very helpful yet)
        elif arg == '-h':
            print "This is TEDView, a viewer for time-based data.  It was developed for \
displaying the activities during processing of dialogues on- and offline along \
with annotations and gold standards.  Events that TEDview can display comprise \
point-events, spans which can be labeled, and audio snippets."
            print "Options:"
            print "    -p Port to listen on"
            print "    -i address to listen for"
	    print "    -vp port your video player is listening to (optional)"
            print "    -o video offset (ask David for further details...)"
            sys.exit();
        else:
            filename = arg
            if not os.path.exists(filename):
                raise ValueError("File not found: %s" % filename)

    # initializing gui:
    tk_root = Tkinter.Tk()
    
    controller = Controller(tk_root, ipaddress, port, vid_offset, vidPort)

    # initializing server:
    spawn_TEDserver(controller, ipaddress, port)
    
    if filename:
        controller.open_file(filename)
        
    tk_root.mainloop()

if __name__ == '__main__':
    main()
