# // --------------------------------------- //
"""
PyGraph is a collection of routines to do some
basic directed graph rendering in pure python.

It is somewhat similar to the Dot & GraphViz 
modules, but without requiring them (or external source)
to be installed.  

It isn't fast, but it *is* portable...

tkgraphview takes care of drawing edges and placing names/etc
	allowing drag/drop, etc.
	
pygraph class renders & places nodes according to different
	models: spring and heirarchical are supported.


(c) Beracah Yankama, 2004-2007

This is NOT GPL code!
"""
# // --------------------------------------- //

import Tkinter


from pygraph import *

class tkGraphView(Tkinter.Canvas):
    # Global identifiers for Application Status
    __AS_CREATE = 1
    __AS_MOVE = 2
    __AS_RESIZE = 3

    # Global identifiers for Dragging Status
    __DS_NODRAG = 1
    __DS_DRAGGING = 2
    
    # nodeEdgeAry is an old-style hack container that we use.  You can see it parsed into the new
    	# node and edge objects below
    # title = window title string
    # outfile = basename of output.ps file == i.e. "myfile" becomes "myfile.ps"
    # root = window/frame for the graph to appear in.  If none, then it assumes a toplevel window
    	# if this graph is the only tk running, then 'onlyroot' must be set to True
    # graphtype = type of graph, valid options: 'directed' and 'spring';  spring is good for undirected/cyclic graphs
    	# whereas directed is good for heirarchical type graphs.
    # startTk = True if the grapher is the only instance of tk running -- will then start up
    	# tk.mainloop on its own.
    
    def __init__(self, nodeEdgeAry, title, outfile='',root=None, graphtype='directed',startTk=False, canvas=None):
        
        if root is None:	# don't have a window to place in.
            if startTk:		# there is no tk instance running
                self._root = Tkinter.Tk()
            else:					# there is a tk instance running, place new window
        		self._root = Tkinter.Toplevel() # (use this one when in main prog)
        else:
            self._root = root

        self.dragmode = self.__DS_NODRAG
        self.imagefile = outfile
        
        self.selected = ''
        
        self.tagNodeMap = {}
        self.nodeTagMap = {}
        self.tagPos = {}    # totally a hack to get the position.

        self.highlightTags = []

        
        # add a "save" button
        self.saveButton = Tkinter.Button(self._root, text='Save', background='#a0f0c0', foreground='black', command=self.writeImage)
        self.saveButton.pack(side='top')
                                   
        self.saveButtonLabel = Tkinter.Label(self._root, text='( '+self.imagefile+'.ps )')
        self.saveButtonLabel.pack(side='top')
        
        
        # choose canvas sizing..
        # ok, we want to check the number of nodes to estimate the size of graph we should make
        # if each node gets 75x75 pixels of area...  then 50% of the area goes unused in rendering...
        # 900+50 3000
        self.xdim = 1350
        self.ydim = 900+50

        self.xdimPicture = self.xdim
        self.ydimPicture = self.ydim
        
        self.totalarea = self.xdim * self.ydim
        if False:#(self.ydim > 600):
            areadiff = self.xdim * (self.ydim-600)
            self.ydim = 600
            self.xdim += areadiff/600
        
        # enforce a max size
        #if (self.xdim > 900):
        #    self.xdim = 900

        if True:
            self.scrollbarX = Tkinter.Scrollbar(self._root, orient=Tkinter.HORIZONTAL)
            self.scrollbarX.pack(side=Tkinter.BOTTOM, fill=Tkinter.X)

            self.scrollbarY = Tkinter.Scrollbar(self._root)
            self.scrollbarY.pack(side=Tkinter.RIGHT, fill=Tkinter.Y)                
                
        if canvas:
            self.canvas = canvas
        else:
            if True:
                # 1400 / 600
                self.canvas = Tkinter.Canvas(self._root, width=self.xdim, height=self.ydim, background='white', xscrollcommand=self.scrollbarX.set, yscrollcommand=self.scrollbarY.set, scrollregion=(0, 0, 1350, 950))
            else:
                self.canvas = Tkinter.Canvas(self._root, width=self.xdim, height=self.ydim, background='white', scrollregion=(0, 0, self.ydim, self.xdim))
                    
            self.canvas.pack(side=Tkinter.LEFT)


        if True:
            self.scrollbarY.config(command=self.canvas.yview)
            self.scrollbarX.config(command=self.canvas.xview)

        # write "loading"
        # draw some test stuff.
        # self.canvas.create_oval(200,200,300,300)
        # we actually want to draw all the nodes on the graph here...
        # so we create a rendering object
        self.graph = Grapher(graphtype,self.canvas, self)

        #self.addNodesFromArray(nodeEdgeAry)
        # integrate the old format edge array   
        
        self.graph.intelligentRandomize()
        
        # just here to watch the bodies behave...

        self.graphType=graphtype

        if graphtype=='directed':
            self.graph.layout(self.canvas,'directed',1)
        if graphtype == 'gridded':
            self.graph.layout(self.canvas,'gridded',1)
        else:
            for i in range(35):
                self.graph.layout(self.canvas,'spring',1)
                self.draw()
                self.canvas.update()

        # scale it and redraw
        self.graph.scaletoCanvas()
        self.draw()

        # write to file
        #self.writeImage()
        
        # setup the dragable nature
        #self.canvas.bind("<Button-1>", self.OnCanvasClicked)
        #self.canvas.bind("<B1-Motion>", self.OnCanvasMouseDrag)
        #self.canvas.bind("<ButtonRelease-1>", self.OnCanvasMouseUp)
        #self.canvas.bind("<Button-3>", self.OnCanvasB3Clicked)
        
        self.dragmode = self.__DS_DRAGGING
        
        if startTk:	# no window was defined, 
            self._root.mainloop()

    def removeAll(self):
        self.selected = ''
        
        self.tagNodeMap = {}
        self.nodeTagMap = {}
        self.tagPos = {}    # totally a hack to get the position.

        self.highlightTags = []

        self.graph.removeAll()

    def addNodesFromArray(self, nodeEdgeAry):
        for cNode in nodeEdgeAry:
            self.graph.addNode(cNode['node'], cNode['label'], cNode['color'], cNode['shape'], x=cNode.get('x'), y=cNode.get('y'), yEquivClassInp=cNode.get('yEquivClass'))
            
            for e in range(len(cNode['edges'])):
                tmpedgename = ''
                edgeColour = "black"
                if e < len(cNode['edgenames']):
                    tmpedgename = cNode['edgenames'][e]
                if e < len(cNode['edgeColour']):
                    edgeColour = cNode['edgeColour'][e]

                if tmpedgename == "SLL":
                    edgeColour = "green"
                    tmpedgename = ''
                elif tmpedgename == "GIL":
                    edgeColour = "orange"
                    tmpedgename = ''
                                    
                #print str(x['node']) + ' ' + str(x['edges'][e]) + ' ' + str(tmpedgename)
                
                self.graph.addEdge(cNode['node'], cNode['edges'][e], tmpedgename, edgeColour)

    def goRight(self, arg):
        while True:
            self.canvas.xview_scroll(.1, "units")

    def width(self):
        return self.xdim

    def height(self):
        return self.ydim

    def writeImage(self):
        # BUG: the size which is given here is wrong. only empty pages to be seen.
        # 75 dpi
        self.canvas.postscript(file='SLLandGRIN_graph.ps', width=self.xdim, height=self.ydim, pagewidth=self.xdim,pageheight=self.ydim)
            
        
        # run system command to conver to .png
        
            
    def destroy_scrollbar(self):
        self.scrollbarX.pack_forget()
        self.scrollbarY.pack_forget()
        #self.saveButton.pack_forget()
        #self.saveButtonLabel.pack_forget()

        # There is a minor memory leak at this point, but it avoides nasty bugs. 
        #self.scrollbarX.destroy()
        #self.scrollbarY.destroy()
        
    def destroy(self):

        self.scrollbarX.pack_forget()
        self.scrollbarY.pack_forget()

        self.scrollbarX.destroy()
        self.scrollbarY.destroy()

        if self._root:
            self._root.destroy()
        if self.canvas:
            self.canvas.destroy()

        self._root = None
        self.canvas = None
        self.graph.destroy()
        self.graph = None        

    def OnCanvasMouseUp(self, event):
        self.selected = ''
        self.clickx = 0.0
        self.clicky = 0.0
        self.draw()
    
    
    def OnCanvasClicked(self,event):
        self.clickx = self.canvas.canvasx(event.x)
        self.clicky = self.canvas.canvasx(event.y)
        self.selected = ''
        
        # there are only specific kinds that can be selected...
        neartags = self.canvas.find_overlapping(self.clickx-20, self.clicky-20, self.clickx+20, self.clicky+20) # find_closest(self.clickx, self.clicky)
        # print neartags
        for tag in neartags:
            if self.selected: continue
            if self.tagNodeMap.has_key(tag):
                self.selected = tag
        
        if self.selected:
            # reposition center to mouse cursor
            pos = self.tagPos[self.selected]
                
            dx = event.x - pos[0]
            dy = event.y - pos[1]

            self.tagNodeMap[self.selected].x = event.x
            self.tagNodeMap[self.selected].y = event.y

            self.canvas.move(self.selected,dx,dy)
            self.tagPos[self.selected] = [event.x, event.y]

            
        else:
            self.selected = ''
            self.clickx = 0.0
            self.clicky = 0.0
            

      
    
    def OnCanvasMouseDrag(self,event):
        if not self.selected: return

        if (self.canvas.find_withtag(self.selected)):
            dx = event.x - self.clickx
            dy = event.y - self.clicky
            self.canvas.move(self.selected, dx, dy);
            self.clickx = event.x
            self.clicky = event.y
                        
            self.tagNodeMap[self.selected].x = event.x
            self.tagNodeMap[self.selected].y = event.y

            # keep the current position up to date.
            self.tagPos[self.selected] = [event.x, event.y]
            self.draw()
            
        else:
            self.selected = ''
            self.clickx = 0.0
            self.clicky = 0.0
            self.canvas.coords(self.selected, self.clickx, self.clicky, event.x, event.y)



    def draw(self):
        # remove all non-node objects from screen
        for tag in self.canvas.find_all():
#            if not self.tagNodeMap.has_key(tag):
            self.canvas.delete(tag)

        self.graph.intelligentRandomize()
        self.graph.newLayout()
        if self.graphType=='directed':
            self.graph.layout(self.canvas,'directed',1)
        if self.graphType == 'gridded':
            self.graph.layout(self.canvas,'gridded',1)
        
        inc = {}
        for node in self.graph.nodes:
            if not node.yEquivClass in inc:
                inc[node.yEquivClass] = 10
            inc[node.yEquivClass] += node.fontSize * (len(node.label)) + 20
            node.x += inc[node.yEquivClass] + 100# + 3 * (len(node.label)-1)
                
        self.graph.scaletoCanvas()

        deadLinkOriginators = []
        # draw the edges first, now that they have been rendered so that the
        # nodes overwrite them on the screen.
        for edge in self.graph.edges:
            nodefrom = self.graph.getNodebyName(edge.fromname)  # inefficient, do this on adding edges.
            nodeto = self.graph.getNodebyName(edge.toname)  # inefficient, do this on adding edges.

            if not nodeto:
                deadLinkOriginators.append(nodefrom)
                
            # prune if either is not found, 
            if (nodefrom and nodeto) :
                # make the arrows visible...

                
                
                self.opp = float(abs(nodefrom.x-nodeto.x))
                self.adj = float(abs(nodefrom.y-nodeto.y))
                self.linelength = ((self.opp)**2 + (self.adj)**2)**.5
                self.hyp = float(self.linelength)
                
                # linelength depends on the width of the end node
                endwidth = 10 + 5 * (len(nodeto.name)-1)
                endheight = 10
                
                if self.adj == 0.0:
                    self.new_opp = float(self.linelength - endwidth)
                    self.new_adj = 0.0
                
                elif self.opp == 0.0:
                    self.new_adj = float(self.linelength - endheight)
                    self.new_opp = 0.0
                    
                else:
                    self.tan_th = float(self.opp/self.adj)
                    self.sine_th = float(self.opp/self.hyp)
                    self.cos_th = float(self.adj/self.hyp)
                    sin_rad = math.asin(self.sine_th)/(.5*3.142)
                    diff = (endwidth - endheight) * (sin_rad**4) + endheight
                    self.newlength = float(self.linelength - diff)
                    self.new_opp = self.sine_th * self.newlength
                    self.new_adj = self.new_opp/self.tan_th
                    
                    
                
                
                # create a curve to the lines...
                xshift = 0
                yshift = 0
                xshift2 = 0
                yshift2 = 0
                xshift3 = 0
                yshift3 = 0
                xshift4 = 0
                yshift4 = 0
                
                labelshiftx = 0
                labelshifty = 0
                
                if edge.label:
                    if nodeto.y > nodefrom.y:
                        xshift = 20
                    if nodeto.y < nodefrom.y:
                        xshift = -20
                    if nodeto.x > nodefrom.x:
                        yshift = -20
                    if nodeto.x < nodefrom.x:
                        yshift = 20


                    labelshiftx = xshift
                    labelshifty = yshift
                
                if nodeto == nodefrom:
                    xshift = .5*self.ydimPicture
                    yshift = 0
                    xshift2 = .05*self.xdimPicture 
                    yshift2 = .05*self.ydimPicture
                    xshift3 =  0
                    yshift3 = .05*self.ydimPicture
                    
                    labelshiftx = .05*self.xdimPicture
                    labelshifty = .05*self.ydimPicture
                elif edge.color == "green":
                    
                    xshift = .01*self.xdimPicture
                    yshift = -.02*self.ydimPicture

                    xshift2 = 0
                    yshift2 = -.03*self.ydimPicture

                    xshift3 = -.01*self.xdimPicture
                    yshift3 = -.02*self.ydimPicture

                    labelshiftx = .02*self.xdimPicture
                    labelshifty = .02*self.ydimPicture

                points = [nodefrom.x,nodefrom.y]

                midshift = FindMidpoint(nodefrom.x+xshift,nodefrom.y+yshift,nodeto.x+xshift,nodeto.y+yshift)
                
                if (xshift2) or (yshift2):
                    points.append(nodefrom.x+xshift)
                    points.append(nodefrom.y+yshift)
                    
                    points.append(nodefrom.x+xshift2)
                    points.append(nodefrom.y+yshift2)
                    
                    points.append(nodefrom.x+xshift3)
                    points.append(nodefrom.y+yshift3)
                else:

                    points.append(midshift[0])
                    points.append(midshift[1])
    
                
                self.direction = -1
                if (nodeto.x > nodefrom.x):
                    self.direction = 1
                    
                self.newx = nodefrom.x + self.direction * self.new_opp
                
                self.direction = -1
                if (nodeto.y > nodefrom.y):
                    self.direction = 1
                    
                self.newy = nodefrom.y + self.direction * self.new_adj

                points.append(self.newx)
                points.append(self.newy)

                self.canvas.create_line(points, arrow='last', smooth='true', fill=edge.color)
                # add label
                if edge.label:
                    self.graph.updateLabelPos(edge) # we always make sure the label is updated..
                    self.canvas.create_text(edge.labelx+labelshiftx, edge.labely+labelshifty, text=edge.label, fill='black',anchor=Tkinter.NW)
        
        
        # draw the nodes now that they have been lay'ed out.
        for node in self.graph.nodes:    
            if self.selected:
                if self.nodeTagMap.has_key(self.selected):
                    if node == self.nodeTagMap[self.selected]:
                        continue
            
            # check if this node is already in tmpnodes
            # just update the position
            if self.nodeTagMap.has_key(node):
                tag = self.nodeTagMap[node]

                pos = self.tagPos[tag]
                
                dx = node.x - pos[0]
                dy = node.y - pos[1]
                
                
                self.canvas.move(tag,dx,dy)
                self.tagPos[tag] = [node.x,node.y]

                # stay on top
                self.canvas.lift(tag)
            # create the objects from new           
            else:
                if node in deadLinkOriginators:
                    pOneX = node.x - node.width/1.0
                    pOneY = node.y
                    pTwoX = node.x
                    pTwoY = node.y + node.height/1.0
                    pThreeX = node.x+node.width/1.0
                    pThreeY = node.y
                    pFourX = node.x
                    pFourY = node.y - node.height/1.0
                    tag = self.canvas.create_polygon(pOneX, pOneY, pTwoX, pTwoY, pThreeX, pThreeY, pFourX, pFourY, fill="red")
                elif node.shape == 'rect':
                    #tag = self.canvas.create_rectangle(node.x-tmpwidth, node.y-10, node.x+tmpwidth, node.y+10, fill=node.color)
                    tag = self.canvas.create_rectangle( (node.x-node.width/2.0) -10, (node.y-node.height/2.0) -10, ( node.x+node.width/2.0) +10 , (node.y+node.height/2.0)+10, fill=node.color)
                else:
                    #tag = self.canvas.create_oval(node.x-tmpwidth, node.y-10, node.x+tmpwidth, node.y+10, fill=node.color)
                    tag = self.canvas.create_oval(node.x-node.width/2.0, node.y-node.height/2.0, node.x+node.width/2.0, node.y+node.height/2.0, fill=node.color)

                # add the tag to the node map
                self.tagNodeMap[tag] = node
                self.nodeTagMap[node] = tag
                self.tagPos[tag] = [node.x,node.y]

            
            # name the node
            self.canvas.create_text(node.x,node.y, text=node.label, font=("Helvetica", str(node.fontSize)))

    def HighlightNode(self,nodename,node_in):
        if not node_in:
            node = self.graph.getNodebyName(nodename)
        else:
            node = node_in
        
        # get the node position
        if node:
            # hack for node width, make dedicated functions/etc.
            tmpwidth = 10 + 5 * (len(node.name)-1)
            
            tag = self.canvas.create_oval(node.x-tmpwidth-3,node.y-13,node.x+tmpwidth+3,node.y+13, outline='orange', fill='yellow', width=2)
            self.canvas.lower(tag)
            
            self.highlightTags.append(tag)
        
        # add another shape behind this one.
        
        
        
    def deHighlightNodes(self):
        for tag in self.highlightTags:
            self.canvas.delete(tag)
        self.highlightTags = []

