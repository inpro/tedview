#!/usr/bin/env python
# -*- coding: utf-8 -*-

# (c) 2008, inpro project
# 
# dumps incrementally sent TEDview-XML to stdout
# press enter to stop.
# usage: ./datadump.py > dump.xml



import sys
import socket
import SocketServer
import thread


ipaddress = '127.0.0.1'
port = 2000

class DataSinkServerHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        xml = self.rfile.read()
	print xml

class ThreadingTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer): pass

if __name__=='__main__':
    # Processing commandline arguments:
    argv = sys.argv[1:]
    while argv:
        arg = argv.pop(0)
        if arg == '-p':
            port = int(argv.pop(0))
        elif arg == '-i':
            ipaddress = argv.pop(0)
    # initializing server:
    server = SocketServer.ThreadingTCPServer((ipaddress, port), DataSinkServerHandler)
    print "<dialogue id=\"test-01\">"
    thread.start_new_thread(server.serve_forever, ())
    sys.stdin.readline()
    print "</dialogue>"

