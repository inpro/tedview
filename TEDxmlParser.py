# -*- coding: utf-8 -*-

from TEDmodel import *
import xml.dom.minidom

class XMLParser:

    def __init__(self):

        pass

    def process_xml(self, xml_str):
        # don't bark on empty reads

        #print "PARSING : \t", xml_str
        if xml_str == '':
            return
        #print "CSTRING:\t",xml_str
        #try:
        event_nodes = []
        nodes = xml.dom.minidom.parseString(xml_str.decode("utf8","replace").encode("ascii","replace")).childNodes
        nodes = [n for n in nodes if n.nodeName != '#comment']

        if nodes[0].nodeName == 'dialogue':
            event_nodes = [n for n in nodes[0].childNodes if (n.nodeName == 'event' or n.nodeName == 'point' or n.nodeName == 'control')]
        elif nodes[0].nodeName=='event' or n.nodeName == 'point' or n.nodeName == 'control':
            event_nodes = nodes
        else:
            raise ValueError("Received message contains neither a dialogue " + 
                                "nor an event element.")

        events = [Event(n) for n in event_nodes]

            #print "EVENTS PARSED:\t",events
        return events
        #except:
            # i.e.: no childnodes have been found
            #pass


    def testForXml(self, string):
        if string.startswith("<?xml version="):
            return True
        return False

    def testForTG(self, string):
        if string.startswith('File type = "ooTextFile"'):
            return True
        return False            
