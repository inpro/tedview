# -*- coding: utf-8 -*-

###############################################################################
#       Data structures describing the dialogue and its constituents:        #
###############################################################################

# Python stdlibs:
import xml.dom.minidom
import Tkinter

from sys import maxint

# 3rd-party libs:
import tkSnack

# TEDview stuff:
import auxiliary_tools as tools

class OrderedChannels:
    def __init__(self):
        self.channels = {}

    def getChannelByName(self, name):
        return self.channels[name][1]
        
    def __len__(self):
        """ Returns the number of Channels in this Tracker. """
        return len(self.channels)

    def values(self):
        # only return the channels themselves at this point without any regards to their order
        return [x for (_,x) in self.channels.values()]

    def has_key(self, key):
        for channelName in self.channels:
            if channelName == key:
                return True
        return False

    def addChannel(self, name, channel, orderNumber=-1):
        if orderNumber == -1:
            orderNumber = len(self.channels)
        self.channels[name] = (orderNumber, channel)

    def clear_channel(self, channel):
        self.channels[channel.name][1].clear()

    def getOrderedChannels(self):
        sortedList = self.channels.values()
        sortedList.sort(cmp=self.thisSort)
        if sortedList == None:
            return []
        return [x for (_,x) in sortedList]
        

    def thisSort(self, left, right):
        a,b = left
        c,d = right
        if a > c:
            return 1
        return -1

class Tracker:
    """ The Tracker is the top-level data structure for all the logical data
    that describe a dialogue.  It's basically a container for Channels. """
    def __init__(self):
        self.channels = OrderedChannels()
        #self.channels = {}
        # FIXME: The model shoudn't have references to Tkinter.
        self.current_time_tkvar = Tkinter.StringVar()
        self.no_events_tkvar = Tkinter.StringVar()
        # channel observers:
        self.channel_observers = []
        self.new_events_observers = []
        self.allChannelsLatestEventsObserver = []

        # .....
        self.set_current_time(0)
        self.start = 0.0
        self.end = 0.0

        self.channels = OrderedChannels()

    # obsolete
    def __len__(self):
        """ Returns the number of Channels in this Tracker. """
        return len(self.channels)
    #
    
    def no_events(self):
        """ Returns the number of events stored by this Tracker. """
        return sum([len(c) for c in self.channels.values()])
    
    def get_channels(self):
        """ Returns a list of all Channels stored by this Tracker."""
        return self.channels.values()
    
    def get_channel(self, name):
        """ Returns the Channel with the specified name.  Raises a KeyError if
        a Channel with the given name is not known.  """
        return self.channels.getChannelByName(name)
    
    def set_current_time(self, time):
        """ Sets the current dialogue time (in milliseconds).  This has no
        effect on the GUI (the Cursor) as the Tracker is designed to be
        ignorant of the GUI.  """
        time = int(time)
        self.current_time_tkvar.set(tools.ms_to_time(time))
        self.current_time = time
        self.notifyAllChannelsLatestEventsObserver()
        for channel in self.channels.values():
            channel.notify_latest_events_observers()
            
    def new_channel(self, name, orderNumber=-1):
        """ Creates, stores, and returns a new Channel with the specified
        name."""
        if not self.has_channel(name):
            channel = Channel(name, self)
            self.channels.addChannel(name, channel, orderNumber)
            self.notify_channel_observers("add", channel)
        return channel
    
    def register_for_channels(self, observer):
        self.channel_observers.append(observer)
    def unregister_for_channels(self, observer):
        self.channel_observers.remove(observer)
    def notify_channel_observers(self, action, channel):
        for observer in self.channel_observers:
            observer(action, channel)
    def has_channel(self, name):
        """ Returns True if the Tracker has a Channel with the given name.
        False is returned otherwise. """
        return self.channels.has_key(name)

    def clear_channel(self, channel):
        if self.channels.has_key(channel):
            self.channels.clear_channel(channel)
            self._clear_channel(channel)
            self.notify_channel_observers("clear", channel)

    def _clear_channel(self, channel):
        """ Removes all Events from the specified Channel.  The Channel itself
        is retained. """
        channel.clear()
        self.start = min((ch.start for ch in self.channels.values()))
        self.end   = max((ch.end for ch in self.channels.values()))
        self.no_events_tkvar.set(self.no_events())

        self.notifyAllChannelsLatestEventsObserver()

    def remove_channel(self, channel):
        """ First clears all Events from a Channel and then removes the
        Channel."""
        # TODO Memory leaks?  Are audio files freed? etc.
        self.clear_channel(channel)
        del self.channels.channels[channel.name]
        self.notify_channel_observers("remove", channel)
        del channel
        self.no_events_tkvar.set(self.no_events())
        
    def register_for_new_events(self, observer):
        self.new_events_observers.append(observer)
        
    def unregister_for_new_events(self, observer):
        try:
            self.new_events_observers.remove(observer)
        except:
            pass        
        
    def notify_new_events_observers(self, event):
        for observer in self.new_events_observers:
            observer(event)
    # note from js: this is ugly. u might expect this event gets dispatched everytime an event is added to a channel
    # from this tracker pane.
    # this is not true. this event is only dispatched if you add an event on the TRACKER, where it is automatically determined
    # to which channel the event has to be added. it does not get called if the event is added on a CHANNEL.
    
    def add_event(self, event):
        """ Adds an Event to the channel with the specified name. """
        if self.has_channel(event.originator):
            channel = self.get_channel(event.originator)
        else:
            channel = self.new_channel(event.originator)
        channel.add_event(event)
        self.start = min(self.start, event.time)
        self.end   = max(self.end, event.endTime)
        self.notify_new_events_observers(event)
        self.no_events_tkvar.set(self.no_events())

    def registerForAllChannelsLatestEvents(self, observer):
        if observer not in self.allChannelsLatestEventsObserver:
            self.allChannelsLatestEventsObserver.append(observer)

    def notifyAllChannelsLatestEventsObserver(self):
        result = []
        for ch in self.channels.getOrderedChannels():
            result.extend(ch.latest_events())

        for obsi in self.allChannelsLatestEventsObserver:
            obsi(result)
        
    def get_duration(self):
        """ Returns the overall duration of the dialogue in milliseconds, i.e.
        the time difference between the beginning of the first Event until the
        end of the last Event. """
        return self.end - self.start

    # this is obviously bad. FIXME! :((
    # - dialogue ID is missing
    # - no formatting. very ugly xml, i.e. if converting from textgrid 
    def serialize_to_xml(self):
        return ('<?xml version="1.0" encoding="UTF-8"?>\n<dialogue id="">\n' + "\n".join(map(lambda c:c.serialize_to_xml(), self.channels.values())) + "</dialogue>\n").encode("utf-8")

class Channel:
    """ A channel contains incremental data of a certain type (well not typed yet) """
    def __init__(self, name, tracker):
        self.name = name
        self.tracker = tracker
        self.events = []
        self.start = 0
        self.end = 0
        self.new_events_observers = []
        self.latest_events_observers = []
        self.last_latest_events = None
        self.content_type = None
    def __len__(self):
        return len(self.events)
    def clear(self):
        self.events = []
        self.start = 0
        self.end = 0
    def add_event(self, event):
        # FIXME: Subevents do not set the content_type of channels.  Why is that?
        if not self.content_type:
            self.content_type = event.content_type
        elif not event.content_type:
            pass
        elif event.content_type != self.content_type:
            errmsg = "Events has content type incopatible with channel: %s" % event
            raise ValueError(errmsg)
            
        self.events.append(event)
        if not self.start:
            self.start = event.time
            # js: take longest subevent duration instead here
            self.end = event.time + event.duration
            self.end = event.endTime
        else:
            self.start = min(self.start, event.time)
            self.end = max(self.end, event.time + event.duration)
            self.end = max(self.end, event.endTime)
        self.notify_new_events_observers(event)
    #
    # Informing observers about changes of the state of affairs:
    #
    # New events:
    def register_for_new_events(self, observer):
        self.new_events_observers.append(observer)
    def unregister_for_new_events(self, observer):
        self.new_events_observers.remove(observer)
        
    def notify_new_events_observers(self, event):
        for observer in self.new_events_observers:
            observer(event)
            
    # Changes of set of latest events with respect to current time:
    def register_for_latest_events(self, observer):
        self.latest_events_observers.append(observer)
    def unregister_for_latest_events(self, observer):
        try:
            self.latest_events_observers.remove(observer)
        except:
            pass
    def notify_latest_events_observers(self):
        latest_events = self.latest_events()
        if self.last_latest_events == latest_events:
            return
        for observer in self.latest_events_observers:
            observer(latest_events)
        self.last_latest_events = latest_events
        
    def latest_events(self):
        """Collects the latest first-order events with respect to current
        time, i.e. cursor time."""
        # All first-order events:
        #events = filter(lambda e:not e.subevent, self.events)
        # js: this is a very weird point to access (private) attributes of the event.
        # if the xml is changed. make sure that the diamond property of the events is correctly attributed.
        events = filter(lambda e: e.diamond, self.events)
        # Find most recent events:
        d_min = maxint
        res = []
        for event in events:
            d = self.tracker.current_time - event.time
            if 0 <= d < d_min:
                res = [event]
                d_min = d
            elif d == d_min:
                res.append(event)
        return res

    # does not work with incremental units yet
    def serialize_to_xml(self):
        top_events = filter(lambda e:not e.subevent, self.events)
        return "\n".join(map(lambda e:e.serialize_to_xml(), top_events))

#
# Data structures describing events:
#

# Classes:

class AbstractEvent(object):
    def __init__(self, xml_or_node):
        if type(xml_or_node) == type(""):
            self.node = xml.dom.minidom.parseString(xml_or_node).childNodes[0]
        else:
            self.node = xml_or_node
    def serialize_to_xml(self):
        xml_str = self.node.toprettyxml(indent='  ');
        # fix minidom's ugly prettyprinting:
        import re;
        text_re1 = re.compile('( *\n)+')
        xml_str = text_re1.sub('\n', xml_str)
        text_re2 = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)    
        xml_str = text_re2.sub('>\g<1></', xml_str)
        return xml_str;

class DataEvent(AbstractEvent):
    def __init__(self, xml_or_node, subevent=False):
        AbstractEvent.__init__(self, xml_or_node)

        self.diamond = False
        
        self.originator = tools.get_attribute_default(self.node, 'originator')
        self.time = int(tools.get_attribute_default(self.node, 'time'))
        self.duration = int(tools.get_attribute_default(self.node, 'duration', 0))
        
        self.isPoint = (self.node.nodeName == 'point');
        self.height = min(1.0, max(0.0, float(tools.get_attribute_default(self.node, 'height', 0.0))))
        
        self.endTime = 0
        possibleEndTimes = [ self.time + self.duration ]
        
        # set the default label color: this is grey for spans and blue for diamonds
        if self.duration > 0 or self.node.tagName == "point":
           defaultColor = 'lightgrey'
        else:
           defaultColor = 'blue'
           self.diamond = True

        
        self.color = tools.get_attribute_default(self.node, 'color', defaultColor)
        # outline_color is only interpreted in toplevel events for now
        self.outline_color = tools.get_attribute_default(self.node, 'outlinecolor', 'black')

        self.SLLs = tools.get_attribute_default(self.node, 'sll', None)
        self.GILs = tools.get_attribute_default(self.node, 'grin', None)
        self.iu_id = tools.get_attribute_default(self.node, 'id', None)

        if self.iu_id != None:
            self.iu_id = int(self.iu_id)

        #if self.SLLs and self.GILs and self.iu_id:
        #    print self.SLLs, self.GILs, self.iu_id

        self.content_type = tools.get_attribute_default(self.node, 'type', None)
        self.subevent = not self.diamond and not self.isPoint

        subevent_nodes = tools.child_nodes_by_name(self.node, 'event')
            
        iu_nodes = tools.child_nodes_by_name(self.node, "iu")

        self.subevents = [SubEvent(n, self.originator) for n in subevent_nodes]
        self.iuEvents = [SubEvent(n, self.originator, str(self.time)) for n in iu_nodes]

        self.subevents += self.iuEvents

        # check whether there's sound attached:
        sound_nodes = tools.child_nodes_by_name(self.node, 'sound')

        for subnode in self.subevents:
            possibleEndTimes.append(subnode.endTime)

        # takes highest value
        for val in possibleEndTimes:
            if val >= self.endTime:
                self.endTime = val
                
        if sound_nodes:
            self.sound = Sound(sound_nodes[0])
            self.duration = self.sound.duration
        else:
            self.sound = None
        # Collect content:
        if subevent:
            self.content = [tools.get_content(self.node)]
        else:
            self.content = tools.list_union([se.content for se in self.subevents])
            
    def __str__(self):
        nodes = tools.all_leaf_nodes(self.node)
        nodes = [n for n in nodes if n.nodeType == 3]
        nodes = [n.nodeValue.strip() for n in nodes]
        nodes = [n for n in nodes if n]
        payload = ' '.join(nodes)
        return payload

class ControlEvent(AbstractEvent):
    def __init__(self, xml_or_node):
        AbstractEvent.__init__(self, xml_or_node)
        self.originator = self.node.getAttribute('originator')
        self.action = tools.get_attribute_default(self.node, 'action', None)

class Sound:
    def __init__(self, node):
        self.resource_location = node.getAttribute('path')
        self.display_type = tools.get_attribute_default(node, 'display', 'waveform')
        #self.tksnack_sound = tkSnack.Sound()
        #self.tksnack_sound.read(self.resource_location)
        # das, 080708: specifying the option "channel" means that
        #  snack won't try to read the whole audio file into memory.
        #  But it also means that the snack widgets (waveform, spectrogram)
        #  can't be used.. and I didn't manage so far to replace them with
        #  normal interval widgets.
        # das, 050908: actually, using "file" is enough to tell snack
        #   not to read the whole sound into memory. Didn't get channel
        #   to work, but it turns out that drawing waveforms is what
        #   causes the problems.
        #self.tksnack_sound = tkSnack.Sound(channel="audio",file=self.resource_location)
        # FIXME: Tk stuff doesn't belong here but to the GUI module.
        self.tksnack_sound = tkSnack.Sound(file=self.resource_location)
        #print self.tksnack_sound.info()
        info = list(self.tksnack_sound.info())
        self.samples = info.pop(0)
        self.frequency = info.pop(0)
        self.max = info.pop(0)
        self.min = info.pop(0)
        self.encoding = info.pop(0)
        self.channels = info.pop(0)
        self.file_format = info.pop(0)
        self.header_size = info.pop(0)
        self.duration = self.samples * 1000.0 / self.frequency
        self.widget = None
    def play(self, *args, **kwargs):
        self.tksnack_sound.play(*args, **kwargs)
    def render(self, scale_factor):
        if self.display_type == 'spectrogram':
            self.widget.create_spectrogram(0, 0,
                sound=self.tksnack_sound, anchor=NW,
                pixelspersecond=scale_factor*1000,
                height=42)
        else:
            self.widget.create_waveform(0, 0,
                sound=self.tksnack_sound,
                anchor=NW,
                pixelspersecond=scale_factor*1000,
                height=42,
                zerolevel=1,
                frame=False,
                subsample=10)

# Some convenient factory functions:

def Event(node):
    if node.nodeName == 'event' or node.nodeName == "point":
        return DataEvent(node)
    elif node.nodeName == 'control':
        return ControlEvent(node)
    
def SubEvent(node, originator, time=None):
    node.setAttribute('originator', originator)
    if time != None:
        node.setAttribute('time', time)
        
    return DataEvent(node, subevent=True)


