#!/usr/bin/env python

import socket
import time
import sys
from xml.dom.minidom import parse

ipaddress = '127.0.0.1'
port = 2000

def events_iter(dom):
    dialogue = dom.getElementsByTagName('dialogue')[0]
    return iter(filter(lambda n:n.localName=='event' or n.localName=='control', dialogue.childNodes))

def feed_events_piecewise(filename, ipaddress, port, timeout=0.1):
    dom = parse(filename)
    for event in events_iter(dom):
        time.sleep(timeout)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipaddress, port))
        s.send(event.toxml().encode('utf8'))
        s.close()

def feed_events(filename, ipaddress, port, timeout=0.1):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ipaddress, port))
    s.send(file(filename).read())
    s.close()

if __name__=='__main__':
    # Processing commandline arguments:
    argv = sys.argv[1:]
    while argv:
        arg = argv.pop(0)
        if arg == '-p':
            port = int(argv.pop(0))
        elif arg == '-i':
            ipaddress = argv.pop(0)
        else:
            filename = arg
    feed_events(filename, ipaddress, port)
