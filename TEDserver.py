# -*- coding: utf-8 -*-
#
# see the file COPYING for license details!
#
# The daemon part of the program:  Listens on specified port and receives
# events from remote clients.
#

import SocketServer
import thread

from TEDmodel import *

def spawn_TEDserver(controller, ipaddress, port):
    def TEDviewServerHandlerWrapper(*args):
        return TEDviewServerHandler(controller, *args)
    server = SocketServer.ThreadingTCPServer((ipaddress, port),
                                             TEDviewServerHandlerWrapper)

    thread.start_new_thread(server.serve_forever, ())
    return server
    
class TEDviewServerHandler(SocketServer.StreamRequestHandler):
    
    def __init__(self, controller, *args, **kwargs):
        self.controller = controller
        SocketServer.StreamRequestHandler.__init__(self, *args, **kwargs)
        
    def handle(self):
        # A livestream is always xml. no textGrid!        

        xml_str = ''
            # HACK in order to allow multiple event-chunks
            # in one stream. events (or dialogues) should be separated
            # by empty lines to trigger the XML-parser
            # empty new lines within XML should be handled gracefully
            # NOTICE: that this should better be done using processing call-backs in SAX
            
            # start out by reading a first line
            # js: it seems like strings sent over a socket become file objects again?
        line = self.rfile.readline()
            # go on reading until the connection is closed
            
        while line != '':
                
            # on contentful lines: add line to xml_str
            if line != "\n":
                xml_str = xml_str + line
            # on empty lines: process xml and empty xml_str
            else:
                try:
                    events = self.controller.xmlP.process_xml(xml_str)
                    for event in events:
                        self.controller.enqueue_new_event(event)
                             
                    xml_str = '' # only remove XML if parsing went alright
                except:
                    # handle parsing problems gracefully
                    # is, and warn the user if it is not related to empty lines.
                    print xml_str
                    xml_str = ''
            line = self.rfile.readline()

        # process last chunk of XML
        if xml_str != '':
            try:
                events = self.controller.xmlP.process_xml(xml_str)
                for event in events:
                    self.controller.enqueue_new_event(event)
            except:
                # handle parsing problems gracefully
                # is, and warn the user if it is not related to empty lines.
                print xml_str
                xml_str = ''
                             
        self.rfile.close()

class ThreadingTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


