simple QT video player that opens video (given as cl argument), and
is controlled via commands sent on socket port 9123 (these commands
are output by TEDview on console, so need to be piped via nc into
the viewer)
