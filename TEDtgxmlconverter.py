import os

class Converter:


    def __init__(self):
        pass

    # wrapper function for the lib
    def convertToXml(self, filename):
        os.chdir("lib")
        print filename
        xmlfobject = os.popen("perl -MTextGrid -e 'TextGrid::printTEDXML(TextGrid::newTextGridFromTextGridFile(\""+filename+"\"))')")
        return xmlfobject.read()

    def convertToTG(self, filename):
        # can be implemented in the future for saving as a TG
        pass
